import json
import logging
import math
import os
from pathlib import Path

import matplotlib.cm
import matplotlib.colors
import matplotlib.patches as patches
import matplotlib.patheffects as patheffects
import matplotlib.pyplot as plt
import numpy as np
from palettable.colorbrewer.sequential import Blues_9, Greens_9, Purples_9, Oranges_9, Reds_9, Greys_9
from palettable.colorbrewer.sequential import YlOrRd_9
from sortedcontainers import SortedDict

STAT_TYPE_TO_TITLE = {
    "packet_number": "PDUs Number (%s)",
    "packet_size": "PDUs Size (%s)",
    "max_rounds": "Max Number of Rounds (%s)",
    "blast_radius": "Blast Radius (%s)",
    "max_scalar_clock": "Max Scalar Clock (%s)",
    "packets_per_round": "Round Waves (%s) (K=%s, R=%s)",
    "topological_distance": "Packets per topological distance (%s) (K=%s, R=%s)"
}

FIGSIZE = (18, 10)

PROTOCOL_COLORS = {
    'Bgp': Greys_9,
    'OpenFabric': Greens_9,
    'Isis': Purples_9,
    'JuniperRift': Oranges_9,
    'Rift': Reds_9
}


class Plotter(object):
    __slots__ = ['templates_path', 'plots_path']

    def __init__(self, base_path):
        current_path = os.path.dirname(Path(__file__).absolute())
        self.templates_path = os.path.join(current_path, "templates")

        self.plots_path = os.path.abspath(os.path.join(base_path, 'plots'))

    def plot_total_packets(self, packet_stats, fontsize=None, legend=True):
        self._plot("packet_number", packet_stats, self._draw_bar, filtering=lambda x: x['avg']['total'][0],
                   figsize=FIGSIZE, fontsize=fontsize, legend=legend)
        self._plot("packet_number", packet_stats, self._draw_bar, filtering=lambda x: x['avg']['total'][0],
                   figsize=FIGSIZE, percentage=True, fontsize=fontsize, legend=legend)

    def plot_size_packets(self, packet_stats, fontsize=None, legend=True):
        self._plot("packet_size", packet_stats, self._draw_bar, filtering=lambda x: x['avg']['total'][1],
                   figsize=FIGSIZE, fontsize=fontsize, legend=legend)
        self._plot("packet_size", packet_stats, self._draw_bar, filtering=lambda x: x['avg']['total'][1],
                   figsize=FIGSIZE, percentage=True, fontsize=fontsize, legend=legend)

    def plot_max_rounds(self, max_rounds_stats, fontsize=None, legend=True):
        self._plot("max_rounds", max_rounds_stats, self._draw_bar, filtering=lambda x: x, figsize=FIGSIZE,
                   fontsize=fontsize, legend=legend)

    def plot_blast_radius(self, packets_per_topological_distance, fontsize=None, legend=True):
        self._plot("blast_radius", packets_per_topological_distance, self._draw_concentric_pie,
                   filtering=lambda x: x, figsize=FIGSIZE, fontsize=fontsize, legend=legend)

    def plot_max_scalar_clock(self, max_scalar_clock_stats, fontsize=None, legend=True):
        self._plot("max_scalar_clock", max_scalar_clock_stats, self._draw_bar,
                   filtering=lambda x: x, figsize=FIGSIZE, fontsize=fontsize, legend=legend)

    def plot_radius_graph(self, lab_info, result_path, run_id, radius_graph, starting_node, failed_neighbours=None,
                          is_normal_starting=False):
        radius_graph_tpl_path = os.path.join(self.templates_path, "radius_graph.tpl.html")
        with open(radius_graph_tpl_path, "r") as radius_graph_tpl_file:
            radius_graph_tpl = radius_graph_tpl_file.read()

        radius_graph_tpl = radius_graph_tpl.replace("%FAILED_NODE%", starting_node)
        radius_graph_tpl = radius_graph_tpl.replace('"%FAILED_NEIGHBOURS%"',
                                                    str(failed_neighbours) if failed_neighbours else "[]")
        radius_graph_tpl = radius_graph_tpl.replace('"%IS_NORMAL_STARTING%"', "true" if is_normal_starting else "false")
        radius_graph_tpl = radius_graph_tpl.replace('"%N_PLANES%"',
                                                    str(int(lab_info['k_leaf'] / lab_info['redundancy_factor'])))
        radius_graph_tpl = radius_graph_tpl.replace('"%GRAPH%"', json.dumps(radius_graph))

        file_name = "radius_graph_%s.html" % run_id
        radius_graph_file_path = os.path.join(result_path, file_name)
        with open(radius_graph_file_path, "w") as radius_graph_file:
            radius_graph_file.write(radius_graph_tpl)

    def plot_rounds_paths_graph(self, result_path, run_id, graph, starting_node):
        round_graph_tpl_path = os.path.join(self.templates_path, "round_graph.tpl.html")
        with open(round_graph_tpl_path, "r") as round_graph_tpl_file:
            round_graph_tpl = round_graph_tpl_file.read()

        round_graph_tpl = round_graph_tpl.replace("%FAILED_NODE%", starting_node)
        round_graph_tpl = round_graph_tpl.replace('"%GRAPH%"', json.dumps(graph))
        round_graph_tpl = self._append_overlay(round_graph_tpl)

        file_name = "rounds_graph_%s.html" % run_id
        round_graph_file_path = os.path.join(result_path, file_name)
        with open(round_graph_file_path, "w") as round_graph_file:
            round_graph_file.write(round_graph_tpl)

    def plot_logical_time(self, result_path, run_id, plot, analysis_time):
        logical_time_tpl_path = os.path.join(self.templates_path, "logical_time.tpl.html")
        with open(logical_time_tpl_path, "r") as logical_time_tpl_file:
            logical_time_tpl = logical_time_tpl_file.read()

        logical_time_tpl = logical_time_tpl.replace('"%PLOT%"', json.dumps(plot))
        logical_time_tpl = self._append_overlay(logical_time_tpl)

        file_name = "logical_time_%s.html" % run_id
        logical_time_file_path = os.path.join(result_path, file_name)
        with open(logical_time_file_path, "w") as logical_time_file:
            logical_time_file.write(logical_time_tpl)

    def _append_overlay(self, plot):
        overlay_tpl_path = os.path.join(self.templates_path, "overlay.tpl.html")
        with open(overlay_tpl_path, "r") as overlay_tpl_file:
            overlay_tpl = overlay_tpl_file.read()

        return plot.replace("%OVERLAY_SCRIPT%", overlay_tpl)

    def _plot(self, stat_type, data, point_callback, filtering, figsize, percentage=False, fontsize=None, legend=True):
        title_template = STAT_TYPE_TO_TITLE[stat_type]
        max_value = -math.inf

        if fontsize is not None:
            matplotlib.rc('font', size=fontsize)

        for scenario, topology_keys in data.items():
            fig = self._init_new_figure(title_template % scenario, figsize=figsize)
            ax = plt.gca()
            ax.set_axisbelow(True)

            topology_protocol_points = {}

            for (k, _, r), protocol_stats in topology_keys.items():
                if (k, r) not in topology_protocol_points:
                    topology_protocol_points[(k, r)] = {}
                for protocol, stats in protocol_stats.items():
                    if 'total' in protocol:
                        continue

                    if protocol not in topology_protocol_points[(k, r)]:
                        topology_protocol_points[(k, r)][protocol] = 0

                    topology_protocol_points[(k, r)][protocol] = filtering(stats)

                    try:
                        max_value = max(max_value, topology_protocol_points[(k, r)][protocol])
                    except TypeError:
                        pass

            for (k, r), protocols_result in topology_protocol_points.items():
                point_callback(protocols_result, r, k, ax, max_value, percentage=percentage)

            self._set_label(ax, 'R', 'K')
            self._set_limits(ax, 18, 0, 0, 20, x_step=2, y_step=2)

            if legend:
                self._set_legend()

            self._draw_heatmap(ax, 16, 16, 0.8, YlOrRd_9.mpl_colormap)

            directory_name = stat_type if not percentage else '%s_percentage' % stat_type
            self._save_fig(fig, "%s_%s" % (scenario, stat_type), directory=directory_name)

            plt.close(fig)

    def _draw_concentric_pie(self, results, x_pos, y_pos, ax, max_value, percentage=False, negative_axis=True):
        size_for_protocol = math.ceil(360 / len(results.keys()))

        current_protocol = 1
        for protocol, values_by_distance in results.items():
            prev_angle = 0 if current_protocol == 1 else size_for_protocol * (current_protocol - 1)
            current_angle = size_for_protocol * current_protocol

            angles = np.linspace(np.radians(prev_angle), np.radians(current_angle))
            xy = np.column_stack([np.cos(angles).tolist(), np.sin(angles).tolist()])

            reversed_values = list(reversed(values_by_distance.values()))
            norm = matplotlib.colors.Normalize(vmin=min(reversed_values), vmax=max(reversed_values))
            cmap = matplotlib.cm.ScalarMappable(norm=norm, cmap=Blues_9.mpl_colormap)

            distance_step_radius = 4200
            for packets in reversed_values:
                if packets > 0:
                    color = cmap.to_rgba(packets)
                    linestyle = 'solid'
                    edgecolor = self._get_light_protocol_color(protocol)
                else:
                    color = 'white'
                    linestyle = 'dashed'
                    edgecolor = 'black'

                ax.scatter([x_pos], [y_pos], marker=xy, s=distance_step_radius, color=color,
                           edgecolors=edgecolor, linestyle=linestyle, alpha=1, zorder=2)

                distance_step_radius -= 1050

            ax.scatter([x_pos], [y_pos], marker=xy, s=200, color='black', alpha=1, zorder=2)

            current_protocol += 1

    def _draw_bar(self, results, x_pos, y_pos, ax, max_value, percentage=False, negative_axis=True):
        bar_width = -0.15
        bar_height = 1.8
        multiplier = (-1 if negative_axis else 1)

        values_sum = sum(results.values())
        n_results = len(results.keys())

        log_max_value = math.log(max_value) if max_value > 1 else 1

        starting_position = x_pos + (multiplier * ((n_results / 2) * bar_width))
        for protocol, value in sorted(results.items(), key=lambda x: x[0]):
            bar_value = math.log(value) if value > 1 else value
            normalized_value = (bar_value / log_max_value) * bar_height

            ax.bar(starting_position, normalized_value, bar_width, bottom=y_pos - bar_height / 2, align="edge",
                   color=self._get_light_protocol_color(protocol), zorder=2)

            label = self.human_readable_number(value) if not percentage else ('%d%%' % ((value / values_sum) * 100))
            txt = ax.annotate(label,
                              (starting_position + (-multiplier * bar_width) / 2 + (multiplier * 0.02),
                               y_pos - ((bar_height - normalized_value) / 2)),
                              horizontalalignment="center",
                              verticalalignment="center",
                              rotation=90,
                              color="white")
            txt.set_path_effects([patheffects.withStroke(linewidth=1, foreground='black')])

            starting_position += -multiplier * bar_width

        ax.add_patch(patches.Rectangle((starting_position, y_pos - bar_height / 2),
                                       -bar_width * n_results, 1.8, linewidth=1, edgecolor=[0.2, 0.2, 0.2],
                                       facecolor='none', zorder=2)
                     )

    @staticmethod
    def _set_legend(position='upper left'):
        legend_elements = []

        for protocol, color in PROTOCOL_COLORS.items():
            color_alpha = Plotter._get_dark_protocol_color(protocol)
            legend_elements.append(patches.Patch(color=color_alpha, label=protocol))

        plt.legend(handles=legend_elements, loc=position)

    def _save_fig(self, fig, name, directory=None, file_format='pdf'):
        name = "%s.%s" % (name, file_format)
        results_path = self.plots_path if not directory else os.path.join(self.plots_path, directory)
        os.makedirs(results_path, exist_ok=True)
        graphic_path = os.path.join(results_path, name)
        logging.info("Saving `%s` plot..." % graphic_path)

        fig.savefig(graphic_path, format=file_format)

    @staticmethod
    def _get_light_protocol_color(name):
        colormap = PROTOCOL_COLORS[name].mpl_colors
        (r, g, b) = colormap[math.ceil(len(colormap) / 2)]

        return r, g, b, 1

    @staticmethod
    def _get_dark_protocol_color(name):
        colormap = PROTOCOL_COLORS[name].mpl_colors
        (r, g, b) = colormap[-1]

        return r, g, b, 1

    @staticmethod
    def _init_new_figure(title, figsize=(6, 6.4)):
        fig = plt.figure(figsize=figsize)
        plt.grid(color='k', linestyle='solid', linewidth=0.1)
        plt.tight_layout()
        plt.title(title)

        return fig

    @staticmethod
    def _set_label(ax, label_x, label_y):
        ax.set_xlabel(label_x)
        ax.set_ylabel(label_y)

    @staticmethod
    def _set_limits(xa, x_start, x_end, y_start, y_end, x_step=2, y_step=1):
        x_range = np.arange(x_start - x_step, x_end, step=x_step * np.sign(x_end - x_start))
        all_divisors = [0] + list(set(y for x in map(lambda x: Plotter.divisors(x), x_range) for y in x)) + [x_start]
        plt.xticks(all_divisors)
        xa.set_xlim(left=all_divisors[-1], right=all_divisors[0])

        y_range = np.arange(y_start, y_end, step=y_step * np.sign(y_end - y_start))
        plt.yticks(y_range)
        xa.set_ylim(bottom=y_range[0], top=y_range[-1])

    @staticmethod
    def _draw_heatmap(ax, x_range, y_range, alpha, cmap):
        n_nodes = SortedDict()

        for k in range(0, x_range + 1):
            for r in range(0, y_range + 1):
                if 0 < k <= r:
                    n_nodes[(k, r)] = np.ceil(5 * (r ** 2) / k)

        power_norm = matplotlib.colors.PowerNorm(0.25, vmin=min(n_nodes.values()), vmax=max(n_nodes.values()))
        for (k, r), nodes in n_nodes.items():
            rect = patches.Rectangle((k - 0.5, r - 0.5), 1, 1, alpha=alpha, zorder=0, facecolor=cmap(power_norm(nodes)))
            ax.add_patch(rect)

    @staticmethod
    def divisors(n):
        divs = {1, n}

        for i in range(2, int(np.sqrt(n)) + 1):
            if n % i == 0:
                divs.update((i, n // i))

        return divs

    @staticmethod
    def human_readable_number(number):
        if number == 0:
            return "0"

        size_name = ("", "K", "M", "G")
        i = int(math.floor(math.log(number, 1000)))
        p = math.pow(1000, i)
        s = round(number / p, 1)
        s = int(s) if (int(repr(s)[-1])) == 0 else s

        return "%s%s" % (s, size_name[i])
