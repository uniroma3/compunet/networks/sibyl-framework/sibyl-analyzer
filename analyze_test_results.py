#!/usr/bin/python3

import json
import logging
import os
import re
import sys
import time
from functools import partial

import coloredlogs

from analysis.blast_radius.blast_radius_analyzer import BlastRadiusAnalyzer
from analysis.collision_domain.collision_domain_analyzer import CollisionDomainAnalyzer
from analysis.logical_time.logical_time_analyzer import LogicalTimeAnalyzer
from analysis.packets.callbacks import count_bgp_packets_after_time, count_isis_packets_after_time, \
    count_rift_packets_after_time, filter_bgp_updates, filter_isis_lsps, filter_rift_ties, \
    build_bgp_label, build_isis_label, build_rift_label
from analysis.packets.dumps_reader import DumpsReader
from analysis.packets.packet_grouper import PacketGrouper
from analysis.packets.packet_parser import parse_bgp_packet, parse_isis_packet, parse_rift_packet, \
    reassemble_bgp_fragments
from analysis.rounds.rounds_analyzer import RoundsAnalyzer
from analysis.statistics.data_structures.network_graph import NetworkGraph
from plotter.plotter import Plotter
from utils import format_headers, parse_analysis_arguments

if __name__ == '__main__':
    args = parse_analysis_arguments()

    logging_format = '[%(asctime)s] %(levelname)s - %(message)s'
    logging_formatter = logging.Formatter(logging_format)

    for handler in logging.root.handlers:
        logging.root.removeHandler(handler)

    # noinspection PyArgumentList
    logging.basicConfig(
        format=logging_format,
        level=logging.INFO,
        handlers=[
            logging.StreamHandler(sys.stdout)
        ]
    )
    coloredlogs.install(fmt=logging_format, level="INFO")

    if not args.selected_path:
        results_directory = os.path.abspath(args.base_path)

        results_dirs = sorted(filter(lambda x: "_results" in x, os.listdir(results_directory)))
    else:
        results_dirs = [args.selected_path]
        results_directory = ""

    logging_handler = None

    for results_dir in results_dirs:
        if logging_handler:
            logging.root.removeHandler(logging_handler)

        result_path = os.path.abspath(os.path.join(results_directory, results_dir))
        logging.info("Analyzing results of %s" % result_path)
        try:
            with open(os.path.join(result_path, 'lab.json'), 'r') as lab_json:
                fat_tree_information = json.loads(lab_json.read())
        except FileNotFoundError:
            logging.warning("Missing lab.json in %s! Skipping..." % results_dir)
            continue

        machines_info = list(fat_tree_information.values())
        # List of list of pod machines
        machines_info[1] = list(machines_info[1].values())
        # Flatten the previous list
        machines_info[1] = [x for y in machines_info[1] for x in y.items()]
        all_machines_info = list(machines_info[0].items())
        # Merge aggregation layer (0) with pods (1)
        all_machines_info.extend(machines_info[1])

        result_path_content = os.listdir(result_path)

        for json_file in filter(lambda x: re.search(r'^run.*\.json', x), result_path_content):
            start_time = time.time()

            run_id = json_file.replace('.json', '')

            logging_handler = logging.FileHandler(os.path.join(result_path, "analysis_log-%s.log" % run_id))
            logging_handler.setFormatter(logging_formatter)
            logging.root.addHandler(logging_handler)

            logging.info("Analyzing run %s" % run_id)

            analysis_results_name = 'analysis_%s' % json_file
            if not args.no_cache:
                if analysis_results_name in result_path_content:
                    logging.info("%s already analyzed, skipping..." % run_id)
                    continue

            # Craft dumps directory from json file name
            dumps_path = os.path.join(result_path, "dumps-%s" % run_id)

            with open(os.path.join(result_path, json_file), 'r') as json_file_content:
                test_info = json.loads(json_file_content.read())

            interface_name_template = "eth%s" if not test_info['is_k8s'] else "net%s"

            packet_counter_function = None
            collision_domain_map_callback = lambda x: x['total'][0]
            packets_filter_callback = None
            labelling_callback = None
            packet_parser_callback = None

            if 'Bgp' in results_dir:
                packet_counter_function = count_bgp_packets_after_time
                packets_filter_callback = filter_bgp_updates
                labelling_callback = build_bgp_label
                packet_parser_callback = parse_bgp_packet
            elif 'Isis' in results_dir or 'OpenFabric' in results_dir:
                packet_counter_function = count_isis_packets_after_time
                collision_domain_map_callback = lambda x: x['lsp'][0] if 'lsp' in x else 0
                packets_filter_callback = filter_isis_lsps
                labelling_callback = build_isis_label
                packet_parser_callback = parse_isis_packet
            elif 'Rift' in results_dir:
                packet_counter_function = count_rift_packets_after_time
                collision_domain_map_callback = lambda x: x['tie'][0] if 'tie' in x else 0
                packets_filter_callback = filter_rift_ties
                labelling_callback = build_rift_label
                packet_parser_callback = parse_rift_packet

            machines_mac_addresses = test_info.pop('machine_mac_addresses')
            statistics = dict(test_info)

            machine_interfaces = {}
            for machine_name, info in all_machines_info:
                if 'server' in machine_name:
                    continue
                machine_interfaces[machine_name] = list(
                    map(lambda x: (interface_name_template % x['number'],
                                   x['ip_address'],
                                   machines_mac_addresses[machine_name][interface_name_template % x['number']]
                                   ),
                        filter(lambda x: 'lo' not in x['collision_domain'], info['interfaces'])
                        )
                )

            analysis_time = 0
            is_normal_starting = False
            if 'failure_time' in test_info:
                analysis_time = test_info['failure_time']
            if 'recover_time' in test_info:
                analysis_time = test_info['recover_time']
                is_normal_starting = True

            if not isinstance(analysis_time, dict):
                analysis_time = {machine_name: analysis_time for machine_name in machine_interfaces.keys()}

            logging.info("Reading Pcap files...")
            filtering_callback = lambda m, ts, p: ts >= analysis_time[m]
            packets, out_packets, unparsed_packets = DumpsReader().read(dumps_path, machine_interfaces,
                                                                        filtering_callback, packet_parser_callback)

            if unparsed_packets:
                if 'Bgp' in results_dir:
                    logging.info("Reassembling BGP fragments...")
                    reassemble_bgp_fragments(unparsed_packets, packets, out_packets)
                else:
                    logging.warning("There are %d unparsed packets." % len(unparsed_packets))

            logging.info("Computing packet stats by machine...")
            packet_counts = packet_counter_function(out_packets, analysis_time)
            statistics['packet_count'] = packet_counts.to_dict()

            logging.info("Computing packet stats by collision domain...")
            cd_analyzer = CollisionDomainAnalyzer(all_machines_info, statistics['packet_count'],
                                                  interface_name_template)
            cd_analyzer.analyze(collision_domain_map_callback)
            statistics['packets_by_collision_domain'] = cd_analyzer.stats.to_dict()

            if 'failed_machine' in test_info:
                logging.info("Create network graph...")
                network_graph = NetworkGraph(all_machines_info)

                failed_neighbours = []
                if 'failed_interfaces' in test_info:
                    interface_name_prefix = "eth" if not test_info['is_k8s'] else "net"
                    failed_interfaces_number = list(map(lambda x: int(x.replace(interface_name_prefix, '')),
                                                        test_info['failed_interfaces']))

                    failed_machine_neighbours = network_graph.edges[test_info['failed_machine']].keys()
                    failed_machine_labels = network_graph.edge_labels[test_info['failed_machine']]
                    for failed_machine_neighbour in failed_machine_neighbours:
                        (_, interface_number) = failed_machine_labels[failed_machine_neighbour]
                        if interface_number in failed_interfaces_number:
                            failed_neighbours.append(failed_machine_neighbour)
                            break

                logging.info("Computing blast radius...")
                blast_radius_analyzer = BlastRadiusAnalyzer(network_graph, cd_analyzer.stats.to_dict())
                blast_radius_analyzer.analyze(test_info['failed_machine'])
                statistics['blast_radius'] = blast_radius_analyzer.stats.to_dict()
                statistics['radius_graph'] = blast_radius_analyzer.radius_graph.to_d3()

                logging.info("Computing packet groups...")
                packet_grouper = PacketGrouper()
                packets_filter_callback = partial(packets_filter_callback, analysis_time)
                packets_by_machine = packet_grouper.get_packets_by_machine(
                    packets,
                    network_graph,
                    interface_name_template,
                    packets_filter_callback
                )
                groups = packet_grouper.get_groups(packets_by_machine)

                logging.info("Computing rounds graph...")
                rounds_normal_starting = is_normal_starting if not failed_neighbours else True
                rounds_analyzer = RoundsAnalyzer()
                rounds_analyzer.analyze(groups, test_info['failed_machine'], rounds_normal_starting, labelling_callback)
                statistics['max_number_of_rounds'] = rounds_analyzer.max_rounds_n
                statistics['packets_per_round'] = rounds_analyzer.packets_per_round
                statistics["round_graph"] = rounds_analyzer.round_paths_graph.to_d3()

                logging.info("Computing Logical Packets Time...")
                logical_time_analyzer = LogicalTimeAnalyzer()
                logical_time_analyzer.analyze(groups, packets_by_machine, labelling_callback)
                statistics['logical_time_plot'] = logical_time_analyzer.logical_time_plot
                statistics['max_scalar_clock'] = logical_time_analyzer.max_scalar_clock

                plotter = Plotter(os.path.abspath("."))
                logging.info("Plotting Radius Graph...")
                plotter.plot_radius_graph(test_info, result_path, run_id, blast_radius_analyzer.radius_graph.to_d3(),
                                          test_info['failed_machine'], failed_neighbours=failed_neighbours,
                                          is_normal_starting=is_normal_starting)

                if len(rounds_analyzer.round_paths_graph.vertices) > 0:
                    logging.info("Plotting Rounds Graph...")
                    plotter.plot_rounds_paths_graph(result_path, run_id, rounds_analyzer.round_paths_graph.to_d3(),
                                                    test_info['failed_machine'] if not rounds_normal_starting else "")

                logging.info("Plotting Logical Time...")
                plotter.plot_logical_time(result_path, run_id, logical_time_analyzer.logical_time_plot, analysis_time)

            analysis_results_path = os.path.join(result_path, analysis_results_name)
            logging.info("Writing results in %s..." % analysis_results_path)

            with open(analysis_results_path, 'w') as analysis_result_file:
                analysis_result_file.write(json.dumps(statistics, indent=4))

            logging.info(format_headers("Execution Time: %s seconds" % (time.time() - start_time)))
