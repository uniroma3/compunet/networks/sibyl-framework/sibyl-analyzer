#!/usr/bin/python3

import argparse
import logging
import os

import coloredlogs

from analysis.data_manipulator.data_manipulator import DataManipulator
from plotter.plotter import Plotter


def plot_results(directory):
    coloredlogs.install(fmt='[%(asctime)s] %(levelname)s - %(message)s', level="INFO")
    logging.info("Loading data from `%s`..." % directory)

    data_manipulator = DataManipulator()
    data_manipulator.read_all_experiments(directory)

    filtered_packets = data_manipulator.get_packets_stats(packet_filter=['update', 'withdrawn', 'tie', 'lsp'])

    packet_stats = data_manipulator.get_avg_values_for_experiment(filtered_packets)

    packets_per_topological_distance_stats = data_manipulator.get_avg_topological_distance_stats()
    max_rounds_stats = data_manipulator.get_avg_max_rounds_stats()
    max_scalar_clock_stats = data_manipulator.get_avg_max_scalar_clock_stats()

    logging.info("Data loaded successfully!")

    plotter = Plotter(os.path.join(directory, '..'))

    plotter.plot_total_packets(packet_stats)
    plotter.plot_size_packets(packet_stats)
    plotter.plot_max_rounds(max_rounds_stats)
    plotter.plot_blast_radius(packets_per_topological_distance_stats)
    plotter.plot_max_scalar_clock(max_scalar_clock_stats)


if __name__ == '__main__':
    parser = argparse.parser = argparse.ArgumentParser()
    parser.add_argument('path', metavar='RESULTS_PATH', nargs=1)
    args = parser.parse_args()

    results_directory = args.path.pop()

    plot_results(os.path.abspath(results_directory))
