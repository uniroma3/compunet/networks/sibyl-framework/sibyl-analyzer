import argparse


def format_headers(message=""):
    footer = "=============================="
    half_message = int((len(message) / 2) + 1)
    second_half_message = half_message

    if len(message) % 2 == 0:
        second_half_message -= 1

    message = " " + message + " " if message != "" else "=="
    return footer[half_message:] + message + footer[second_half_message:]


def parse_analysis_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('--no_cache', action='store_true', default=False, required=False)

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('selected_path', metavar='PATH', nargs='?')
    group.add_argument('--base_path', metavar='BASE_PATH')

    return parser.parse_args()
