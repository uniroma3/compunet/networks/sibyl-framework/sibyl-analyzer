# Sibyl Analyzer

A tool to analyze and plot the results of the experiments of the [Sybil Framework](https://gitlab.com/uniroma3/compunet/networks/sibyl-framework/sibyl). 

## How does it work?
It takes as input the set of .pcap files containing the packets exchanged by the
nodes during an experiment of the Sibyl Methodologt (see the Sibyl paper for further deatails). 

It computes the metrics (Messaging Load, Locality, Rounds), saving them in a .json file. 
It also generates three interactive .html files containing the topology labeled with the number of packets on each link, the node-state
timeline, and the node-state graph. 

**N:B.** Sibyl Analyzer can also be used to analyze data captured on physical nodes.

## Usage 
You can run sequential analysis running the `analyze_test_results.py` file. It accepts as mandatory input one of the two
following parameters:

- `PATH`: The path of an experiment result to analyze.
- `--base_path`: The path to a folder containing several tests results (e.g. the `analysis_results` folder).

By default, if the analyzer already find analysis result for an experiment it skip to analyze it. To avoid this
behaviour you can specify the `--no-cache` parameter.

You can also run **parallel analysis** using the `parallel_analyze_test_results.py` script. It takes the same parameters
of `analyze_test_results.py` but it performs the analysis in parallel.

The analysis produces a `.json` file for each experiment containing all the values of the computed metrics (Messaging
Load, Locality and Rounds, see the paper for further explanation on the metrics) and 3 `.html` files:

1) The 'radius-graph' containing the topology labeled with the number of packets on each link;
2) The 'node-state timeline' describing how the interactions of nodes evolves over time;
3) The 'node-state graph' describing the state changes of the nodes during the experiment.

### Plots of the Analysis

Sibyl also provides tools to plot the results of the analysis. To run the plotter enter the Sibyl Analyzer folder and
run `plot_test_results.py` specifying the analysis results folder:

```shell
 python3 plot_test_results.py ../analysis_results/
```

It generates plots with comparative analysis of the protocol implementations, giving a clear idea on how different
implementations react to a failure or a recovery
