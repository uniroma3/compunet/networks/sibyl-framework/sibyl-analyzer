import logging

from sortedcontainers import SortedDict

from analysis.statistics.data_structures.graph import Graph


class RoundsAnalyzer(object):
    __slots__ = ["round_paths_graph", "max_rounds_n", "packets_per_round"]

    def __init__(self):
        self.round_paths_graph = Graph()
        self.max_rounds_n = -1
        self.packets_per_round = SortedDict()

    def analyze(self, groups, starting_node, is_normal_starting, labelling_callback):
        self._build_graph(starting_node, is_normal_starting, groups, labelling_callback)

        if len(self.round_paths_graph.vertices) <= 0:
            logging.warning("Round graph is empty.")
            return

        self._get_packets_per_round()

    def _build_graph(self, starting_node, is_normal_starting, groups, labelling_callback):
        if not is_normal_starting:
            for (source, _, _) in groups[0]:
                self.round_paths_graph.add_edge(starting_node, '%s-%d' % (source, 0), weight=0)

        for idx, group in enumerate(groups):
            for (source, dest, p) in group:
                source_name = '%s-%d' % (source, idx)
                dest_name = '%s-%d' % (dest, idx + 1)

                packet_label = labelling_callback(p)

                if self.round_paths_graph.edge_exists(source_name, dest_name):
                    self.round_paths_graph.edges[source_name][dest_name] += 1
                    self.round_paths_graph.edge_labels[source_name][dest_name].append(packet_label)
                else:
                    self.round_paths_graph.add_edge(source_name, dest_name, weight=1)
                    self.round_paths_graph.add_label(source_name, dest_name, label=[packet_label])

    def _get_packets_per_round(self):
        # Get source vertices
        source_vertices = []
        for vertex in self.round_paths_graph.vertices:
            (inbound_edges, outbound_edges) = self.round_paths_graph.get_vertex_edges(vertex)
            if not inbound_edges and outbound_edges:
                source_vertices.append(vertex)

        # For each node compute the max distance to it from sources
        node_distances = {}
        is_failed_node = False
        for source in source_vertices:
            if '-' not in source:
                is_failed_node = True
            distances = self.round_paths_graph.compute_maximum_distances(source)
            for node, distance in distances.items():
                # Avoids negative numbers
                d = distance - 1 if is_failed_node else distance
                if d == -1:
                    continue
                if node in node_distances:
                    node_distances[node] = max(d, node_distances[node])
                else:
                    node_distances[node] = d

        # Distance of nodes represents the round of them, so for each node at distance x counts the inbound edges value
        # y and add to the round x the value y
        for node, distance in node_distances.items():
            if distance not in self.packets_per_round:
                self.packets_per_round[distance] = 0

            round_packets_for_node = 0
            for edge in self.round_paths_graph.get_vertex_inbound_edges(node):
                round_packets_for_node += edge[2]

            self.packets_per_round[distance] += round_packets_for_node

        self.max_rounds_n = max(self.packets_per_round.keys())
