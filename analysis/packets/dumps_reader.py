import logging
import os
import time
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool

import dpkt

from .packet_parser import add_packet_info


class DumpsReader(object):
    @staticmethod
    def get_interface_stream(dumps_path, machine_name, interface):
        path = os.path.join(dumps_path, '%s/%s.pcap' % (machine_name, interface))
        return path

    def read(self, dumps_path, machine_interfaces, filtering_callback, payload_parser):
        start_time = time.time()
        packets = {}
        out_packets = {}

        unparsed_packets = {}

        def pool_callback(item):
            (machine_name, interfaces) = item
            machine_packets = {}
            machine_out_packets = {}

            machine_unparsed_packets = {}

            for (interface, ip_address, mac) in interfaces:
                stream = self.get_interface_stream(dumps_path, machine_name, interface)
                interface_packets = []
                interface_out_packets = []

                interface_unparsed_packets = []

                with open(stream, 'rb') as stream_file:
                    pcap_file = dpkt.pcap.Reader(stream_file)
                    for timestamp, pkt in pcap_file:
                        if filtering_callback(machine_name, timestamp, pkt):
                            raw_packet = dpkt.ethernet.Ethernet(pkt)

                            pkts, unparsed_pkts = payload_parser(raw_packet)

                            if unparsed_pkts:
                                interface_unparsed_packets.append((unparsed_pkts, timestamp, raw_packet.src))

                            add_packet_info(pkts, raw_packet.src, timestamp, interface_packets,
                                            interface_out_packets, mac)

                machine_packets[(interface, mac)] = interface_packets
                machine_out_packets[interface] = interface_out_packets

                if interface_unparsed_packets:
                    machine_unparsed_packets[(interface, mac)] = interface_unparsed_packets

            packets[machine_name] = machine_packets
            out_packets[machine_name] = machine_out_packets

            if machine_unparsed_packets:
                unparsed_packets[machine_name] = machine_unparsed_packets

        pool = Pool(cpu_count())
        pool.map(func=pool_callback, iterable=machine_interfaces.items())

        logging.info("Packets read in %s seconds" % (time.time() - start_time))

        return packets, out_packets, unparsed_packets
