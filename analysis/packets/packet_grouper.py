import logging
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool

from sortedcontainers import SortedList


class PacketGrouper(object):
    def get_groups(self, packets_by_machine):
        packets_by_machine_copy = {}

        for key, value in packets_by_machine.items():
            packets_by_machine_copy[key] = value.copy()

        packets_by_machine = packets_by_machine_copy

        groups = []

        starting_group = []
        for starting_nodes in self._get_starting_nodes(packets_by_machine):
            starting_group.extend(
                self._get_consecutive_packets_by_node(packets_by_machine, starting_nodes, is_sent=True)
            )

        groups.append((starting_group, False))

        # Last group not visited
        while not groups[-1][1]:
            (group_entries, _) = groups[-1]

            self._remove_visited_group_packets(packets_by_machine, group_entries)

            new_group = self._create_new_group(packets_by_machine, group_entries)

            groups[-1] = (group_entries, True)

            # Add group
            if new_group:
                groups.append((new_group, False))

        for (machine_name, packs) in packets_by_machine.items():
            if len(packs) > 0:
                logging.warning("%d packets of `%s` were not used during computation." % (len(packs), machine_name))

        return list(map(lambda x: x[0], groups))

    @staticmethod
    def get_packets_by_machine(packets, network_graph, interface_name_template, packet_filter):
        machine_packets = {}
        interface_name_template = interface_name_template % ""

        def pool_callback(item):
            (machine_name, interface_packets) = item
            neighbours = network_graph.get_vertex_neighbours(machine_name, True)

            machine_packets[machine_name] = SortedList(key=lambda x: x[2].time)

            for (interface_name, mac), packs in interface_packets.items():
                interface_number = int(interface_name.replace(interface_name_template, ""))
                current_neighbour = list(filter(lambda x: x[1][1][1] == interface_number, neighbours))[0]

                for p in packs:
                    if packet_filter(machine_name, p):
                        entry = (machine_name, current_neighbour[0], p) if p.src == mac else \
                            (current_neighbour[0], machine_name, p)
                        machine_packets[machine_name].add(entry)

        pool = Pool(cpu_count())
        pool.map(func=pool_callback,
                 iterable=packets.items())

        return machine_packets

    @staticmethod
    def _get_starting_nodes(packets_by_machine):
        starting_nodes = []

        for machine_name, entries in packets_by_machine.items():
            if len(entries) > 0 and entries[0][0] == machine_name:
                starting_nodes.append(machine_name)

        return starting_nodes

    @staticmethod
    def _get_consecutive_packets_by_node(packets_by_machine, node_name, is_sent):
        consecutive_packets = []

        for (source, dest, p) in packets_by_machine[node_name]:
            if is_sent:
                if source == node_name:
                    consecutive_packets.append((source, dest, p))
                else:
                    break
            else:
                if dest == node_name:
                    consecutive_packets.append((source, dest, p))
                else:
                    break

        return consecutive_packets

    @staticmethod
    def _remove_visited_group_packets(packets_by_machine, group_entries):
        for (source, dest, p) in group_entries:
            packets_by_machine[source].discard((source, dest, p))

            for entry_idx, (machine_source, machine_dest, machine_p) in enumerate(packets_by_machine[dest]):
                if machine_source == source and machine_dest == dest and bytes(p) == bytes(machine_p):
                    packets_by_machine[dest].pop(entry_idx)
                    break

    def _create_new_group(self, packets_by_machine, group_entries):
        new_group = []
        visited = set()

        for (source, dest, _) in group_entries:
            if dest not in visited:
                new_group.extend(self._get_consecutive_packets_by_node(packets_by_machine, dest, is_sent=True))
                visited.add(dest)

        return new_group
