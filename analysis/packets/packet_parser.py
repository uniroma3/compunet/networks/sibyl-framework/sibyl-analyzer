import logging
from copy import copy

import dpkt

from dissector.bgp import Bgp
from dissector.isis import Isis
from dissector.rift import Rift


def mac_to_str(mac_bytes):
    return ':'.join('%02x' % dpkt.compat_ord(b) for b in mac_bytes)


def parse_bgp_packet(packet):
    payload_packet = []
    unparsed_packets = None

    if isinstance(packet.data, dpkt.ip.IP):
        ip = packet.data
        if isinstance(ip.data, dpkt.tcp.TCP):
            tcp = ip.data
            if tcp.sport == 179 or tcp.dport == 179:
                payload_packet, unparsed_packets = parse_bgp_payload(tcp.data)

    return payload_packet, unparsed_packets


def parse_bgp_payload(data):
    payload_packet = []
    unparsed_packets = None

    while len(data) > 0:
        try:
            bgp = Bgp(data)
            payload_packet.append(bgp)
            data = data[bgp.length:]
        except:
            unparsed_packets = copy(data)
            data = data[len(data):]

    return payload_packet, unparsed_packets


def parse_isis_packet(packet):
    payload_packet = []
    unparsed_packets = None

    if isinstance(packet.data, dpkt.llc.LLC):
        llc = packet.data
        if llc.ssap == 0xfe:
            try:
                payload_packet.append(Isis(llc.data))
            except:
                unparsed_packets = copy(llc.data)

    return payload_packet, unparsed_packets


def parse_rift_packet(packet):
    payload_packet = []
    unparsed_packets = None

    if isinstance(packet.data, dpkt.ip.IP):
        ip = packet.data
        if isinstance(ip.data, dpkt.udp.UDP):
            udp = ip.data
            if udp.data[0] == 0xa1 and udp.data[1] == 0xf7:
                try:
                    payload_packet.append(Rift(udp.data))
                except:
                    unparsed_packets = copy(udp.data)

    return payload_packet, unparsed_packets


def add_packet_info(pkts, p_mac, ts, iface_packets, iface_out_packets, iface_mac):
    for packet in pkts:
        packet.src = mac_to_str(p_mac)
        packet.time = ts

        iface_packets.append(packet)

        if packet.src == iface_mac:
            iface_out_packets.append(packet)


def reassemble_bgp_fragments(unparsed_packets, packets, out_packets):
    for machine, interface_fragments in unparsed_packets.items():
        for (interface, mac), iface_fragments in interface_fragments.items():
            init_fragments = list(filter(lambda x: _starts_with_marker(x[0]), iface_fragments))
            fragments = [item for item in iface_fragments if item not in init_fragments]

            while True:
                _assemble_bgp_iface_fragments(machine, interface, mac, init_fragments, fragments, packets, out_packets)

                remaining_fragments = init_fragments + fragments
                new_remaining_fragments = remaining_fragments

                if init_fragments and fragments:
                    _assemble_bgp_iface_fragments(machine, interface, mac, init_fragments, fragments, packets,
                                                  out_packets)

                    new_remaining_fragments = init_fragments + fragments

                if remaining_fragments == new_remaining_fragments:
                    break

            if new_remaining_fragments:
                logging.warning(
                    "%s on %s: Not all packets correctly assembled (%d)" %
                    (machine, interface, len(new_remaining_fragments))
                )


def _starts_with_marker(p):
    return b'\xff' * 16 in p[:16] or b'\xff' * len(p) in p


def _assemble_bgp_iface_fragments(machine, interface, mac, init_fragments, fragments, packets, out_packets):
    for init_pkt, init_timestamp, init_mac in init_fragments.copy():
        for p, p_timestamp, p_mac in fragments.copy():
            if init_mac != p_mac:
                continue

            assembled_packet = init_pkt + p
            pkts, unparsed_bytes = parse_bgp_payload(assembled_packet)

            if pkts:
                add_packet_info(pkts, p_mac, p_timestamp, packets[machine][(interface, mac)],
                                out_packets[machine][interface], mac)

                init_fragments.remove((init_pkt, init_timestamp, init_mac))
                fragments.remove((p, p_timestamp, p_mac))

                if unparsed_bytes:
                    if _starts_with_marker(unparsed_bytes):
                        init_fragments.append((unparsed_bytes, p_timestamp, p_mac))
                    else:
                        fragments.append((unparsed_bytes, p_timestamp, p_mac))

                break
