from multiprocessing import cpu_count
from multiprocessing.dummy import Pool

from analysis.statistics.machine_statistic import MachineStatistic


class DumpsAnalyzer(object):
    def pool_analyze(self, packets, callback=None, packet_filter=None, kwargs=None):
        if kwargs is None:
            kwargs = {}

        has_statistics = 'statistics' in kwargs

        if has_statistics:
            machine_statistics = {}

        for machine_name, interfaces in packets.items():
            if has_statistics:
                machine_statistics[machine_name] = MachineStatistic()

        def packet_callback(p, m, i):
            if callback:
                if has_statistics:
                    new_kwargs = kwargs.copy()
                    new_kwargs['statistics'] = machine_statistics[m]
                else:
                    new_kwargs = kwargs

                callback(p, m, i, **new_kwargs)

        def pool_callback(item):
            (machine, interface_packets) = item

            for interface_name, packs in interface_packets.items():
                for packet in packs:
                    if packet_filter is None or packet_filter(machine, packet):
                        packet_callback(packet, machine, interface_name)

        pool = Pool(cpu_count())
        pool.map(func=pool_callback,
                 iterable=packets.items())

        if has_statistics:
            for _, stats in machine_statistics.items():
                kwargs['statistics'].update(stats)
