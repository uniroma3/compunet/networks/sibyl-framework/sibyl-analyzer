from analysis.packets.dumps_analyzer import DumpsAnalyzer
from analysis.statistics.machine_statistic import MachineStatistic
from dissector.rift import rift_types
from dissector.thrift.constants import direction_str


# BGP CALLBACKS
def count_bgp_packets(packet, machine_name, interface, statistics):
    packet_len = len(packet)

    if packet.type == 1:
        statistics.update_for_interface(machine_name, interface, 'open', packet_size=packet_len)
    elif packet.type == 4:
        statistics.update_for_interface(machine_name, interface, 'keep_alive', packet_size=packet_len)
    elif packet.type == 2:
        if len(packet.withdrawn) > 0:
            # It's a withdraw UPDATE
            statistics.update_for_interface(machine_name, interface, 'withdrawn', packet_size=packet_len)
        else:
            # It's a normal update
            statistics.update_for_interface(machine_name, interface, 'update', packet_size=packet_len)
    else:
        statistics.update_for_interface(machine_name, interface, 'others', packet_size=packet_len)


def filter_bgp_updates(time, machine, packet):
    return packet.type == 2 and packet.time >= time[machine]


def count_bgp_packets_after_time(packets, filter_time):
    analyzer = DumpsAnalyzer()

    statistics = MachineStatistic()

    count_update = lambda m, p: p.type == 2 and p.time >= filter_time[m]

    analyzer.pool_analyze(packets,
                          packet_filter=count_update,
                          callback=count_bgp_packets,
                          kwargs={'statistics': statistics}
                          )

    return statistics


def build_bgp_label(packet, **kwargs):
    label = {'time': float(packet.time)}

    if packet.type == 1:
        label['type'] = "Open"
    elif packet.type == 4:
        label['type'] = "KeepAlive"
    if packet.type == 2:
        label['type'] = "Update"

        routes = []
        wd_routes = []

        if packet.withdrawn:
            wd_routes.extend(packet.withdrawn)
        if packet.announced:
            routes.extend(packet.announced)

        if wd_routes:
            label['type'] += ' (WD)'
            label['WD Routes'] = ", ".join(wd_routes)
        elif routes:
            label['Routes'] = ", ".join(routes)
        else:
            label['End-of-RIB'] = True

    if 'clock' in kwargs:
        label['Logical Clock'] = kwargs['clock']

    return label


# ISIS/OPEN FABRIC CALLBACKS
def count_isis_packets(packet, machine_name, interface, statistics):
    packet_len = len(packet)

    if packet.type in [15, 16, 17]:
        statistics.update_for_interface(machine_name, interface, 'hello', packet_size=packet_len)
    elif packet.type in [10, 18, 20]:
        statistics.update_for_interface(machine_name, interface, 'lsp', packet_size=packet_len)
    elif packet.type in [24, 25]:
        statistics.update_for_interface(machine_name, interface, 'csnp', packet_size=packet_len)
    elif packet.type in [26, 27]:
        statistics.update_for_interface(machine_name, interface, 'psnp', packet_size=packet_len)
    else:
        statistics.update_for_interface(machine_name, interface, 'others', packet_size=packet_len)


def filter_isis_lsps(time, machine, packet):
    return packet.type in [10, 18, 20] and packet.time >= time[machine]


def count_isis_packets_after_time(packets, filter_time):
    analyzer = DumpsAnalyzer()

    statistics = MachineStatistic()

    count_lsps = lambda m, p: p.type in [10, 18, 20, 24, 25, 26, 27] and p.time >= filter_time[m]

    analyzer.pool_analyze(packets,
                          packet_filter=count_lsps,
                          callback=count_isis_packets,
                          kwargs={'statistics': statistics}
                          )

    return statistics


def build_isis_label(packet, **kwargs):
    label = {'time': float(packet.time)}

    if packet.type in [15, 16, 17]:
        label['type'] = "Hello"
    elif packet.type in [18, 20]:
        label['type'] = "LSP"

        label['LSP ID'] = packet.lspid
        label['Seq.N'] = packet.seqnum
        label['Orig.'] = packet.dynamic_hostname
    elif packet.type == 10:
        label['type'] = "FS-LSP"

        label['LSP ID'] = packet.lspid
        label['Seq.N'] = packet.seqnum
        label['Orig.'] = packet.dynamic_hostname
        label['Scope'] = packet.scope
        label['Priority'] = packet.priority
    elif packet.type in [24, 25]:
        label['type'] = "CSNP"
    elif packet.type in [26, 27]:
        label['type'] = "PSNP"

    if 'clock' in kwargs:
        label['Logical Clock'] = kwargs['clock']

    return label


# RIFT CALLBACKS
def count_rift_packets(packet, machine_name, interface, statistics):
    packet_len = len(packet)

    if packet.type == rift_types['LIE']:
        statistics.update_for_interface(machine_name, interface, 'lie', packet_size=packet_len)
    elif packet.type == rift_types['TIDE']:
        statistics.update_for_interface(machine_name, interface, 'tide', packet_size=packet_len)
    elif packet.type == rift_types['TIRE']:
        statistics.update_for_interface(machine_name, interface, 'tire', packet_size=packet_len)
    elif packet.type == rift_types['TIE']:
        statistics.update_for_interface(machine_name, interface, 'tie', packet_size=packet_len)
    else:
        statistics.update_for_interface(machine_name, interface, 'others', packet_size=packet_len)


def filter_rift_ties(time, machine, packet):
    return packet.type == rift_types['TIE'] and packet.time >= time[machine]


def count_rift_packets_after_time(packets, filter_time):
    analyzer = DumpsAnalyzer()

    statistics = MachineStatistic()

    count_ties = lambda m, p: p.type in [rift_types['TIRE'], rift_types['TIDE'], rift_types['TIE']] \
                              and p.time >= filter_time[m]

    analyzer.pool_analyze(packets,
                          packet_filter=count_ties,
                          callback=count_rift_packets,
                          kwargs={'statistics': statistics}
                          )

    return statistics


def build_rift_label(packet, **kwargs):
    label = {'time': float(packet.time)}

    if packet.type == rift_types['LIE']:
        label['type'] = "LIE"
    elif packet.type == rift_types['TIDE']:
        label['type'] = "TIDE"
    elif packet.type == rift_types['TIRE']:
        label['type'] = "TIRE"
    elif packet.type == rift_types['TIE']:
        ser_object_tie_element = packet.serialized_object.content.tie.element

        if ser_object_tie_element.node is not None:
            label['type'] = "Node TIE"
        elif ser_object_tie_element.prefixes is not None:
            label['type'] = "Prefix TIE"
        elif ser_object_tie_element.positive_disaggregation_prefixes is not None:
            label['type'] = "Pos. Disagg. TIE"
        elif ser_object_tie_element.negative_disaggregation_prefixes is not None:
            label['type'] = "Neg. Disagg. TIE"
        elif ser_object_tie_element.external_prefixes is not None:
            label['type'] = "Ext. TIE"
        elif ser_object_tie_element.positive_external_disaggregation_prefixes is not None:
            label['type'] = "Ext. Pos. Disagg. TIE"

        label['Dir.'] = direction_str(packet.serialized_object.content.tie.header.tieid.direction)
        label['Seq.N'] = packet.serialized_object.content.tie.header.seq_nr
        label['Orig.'] = packet.serialized_object.content.tie.header.tieid.originator

    if 'clock' in kwargs:
        label['Logical Clock'] = kwargs['clock']

    return label
