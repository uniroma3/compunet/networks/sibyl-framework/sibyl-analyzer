from multiprocessing import cpu_count
from multiprocessing.dummy import Pool

from ..statistics.generic_statistic import GenericStatistic


class CollisionDomainAnalyzer(object):
    __slots__ = ['fat_tree_information', 'packet_stats', 'interface_name_template', 'stats']

    def __init__(self, fat_tree_information, packet_stats, interface_name_template):
        self.fat_tree_information = fat_tree_information
        self.packet_stats = packet_stats
        self.interface_name_template = interface_name_template

        self.stats = GenericStatistic()

    def analyze(self, map_callback):
        machine_stats = self._count_packets_by_collision_domain(map_callback)

        for _, machine_collision_domain_stats in machine_stats.items():
            self.stats.update(machine_collision_domain_stats)

    def _count_packets_by_collision_domain(self, map_callback):
        machine_stats = {}

        def pool_callback(item):
            (machine_name, info) = item
            if machine_name not in self.packet_stats:
                return

            machine_stats[machine_name] = self._count_collision_domain_packets_from_interfaces(machine_name,
                                                                                               info['interfaces'],
                                                                                               map_callback)

        pool = Pool(cpu_count())
        pool.map(func=pool_callback,
                 iterable=self.fat_tree_information
                 )

        return machine_stats

    def _count_collision_domain_packets_from_interfaces(self, machine_name, machine_interfaces, map_callback):
        stats = GenericStatistic()

        for interface_info in filter(lambda x: 'lo' not in x['collision_domain'], machine_interfaces):
            collision_domain = interface_info['collision_domain']

            interface_name = self.interface_name_template % interface_info['number']
            if interface_name not in self.packet_stats[machine_name]:
                continue

            stats.update_by_key(collision_domain, map_callback(self.packet_stats[machine_name][interface_name]))

        return stats
