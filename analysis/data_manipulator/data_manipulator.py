import copy
import json
import logging
import os
from statistics import mean

from sortedcontainers import SortedDict

from ..statistics.machine_statistic import MachineStatistic


class DataManipulator(object):
    __slots__ = ['scenario_topology_protocol']

    def __init__(self):
        self.scenario_topology_protocol = {}

    def read_all_experiments(self, results_path):
        results_dirs = sorted(filter(lambda x: "_results" in x, os.listdir(results_path)))

        for result in results_dirs:
            result_path = os.path.join(results_path, result)
            logging.info("Loading data from `%s`..." % result_path)
            self._read_all_json_analysis_for_experiment(result_path)

    def _read_all_json_analysis_for_experiment(self, result_path):
        for file_name in os.listdir(result_path):
            if 'analysis' in file_name and '.json' in file_name:
                analysis_json_path = os.path.join(result_path, file_name)
                self._read_experiment_result(analysis_json_path)

    def _read_experiment_result(self, analysis_json_path):
        directory_name = os.path.basename(os.path.dirname(analysis_json_path))
        protocol, scenario = self._extract_protocol_scenario_from_directory_name(directory_name)
        k_leaf, k_top, r = self._extract_topology_params_from_directory_name(directory_name)

        topology_key = (k_leaf, k_top, r)

        with open(analysis_json_path, 'r') as analysis_file:
            experiment_result = json.loads(analysis_file.read())

        if scenario not in self.scenario_topology_protocol:
            self.scenario_topology_protocol[scenario] = {}
        if topology_key not in self.scenario_topology_protocol[scenario]:
            self.scenario_topology_protocol[scenario][topology_key] = {}
        if protocol not in self.scenario_topology_protocol[scenario][topology_key]:
            self.scenario_topology_protocol[scenario][topology_key][protocol] = []

        self.scenario_topology_protocol[scenario][topology_key][protocol].append(experiment_result)

        return experiment_result

    def _extract_protocol_scenario_from_directory_name(self, directory_name):
        protocol = self._extract_protocol_from_directory_name(directory_name)
        scenario = directory_name.replace(protocol, '').split('-')[0]
        return protocol, scenario

    @staticmethod
    def _extract_protocol_from_directory_name(directory_name):
        if 'Bgp' in directory_name:
            return 'Bgp'
        elif 'Isis' in directory_name:
            return 'Isis'
        elif 'OpenFabric' in directory_name:
            return 'OpenFabric'
        elif 'JuniperRift' in directory_name:
            return 'JuniperRift'
        elif 'Rift' in directory_name:
            return 'Rift'
        else:
            raise Exception('Directory name not valid.')

    @staticmethod
    def _extract_topology_params_from_directory_name(directory_name):
        topology_params = directory_name.split('-')[-1].split('_')[:3]
        return int(topology_params[0]), int(topology_params[1]), int(topology_params[2])

    def get_packets_stats(self, node_filter=None, packet_filter=None, protocol_filter=None):
        logging.info("Computing packet count/size statistics...")

        packets_stats_data = {}

        for scenario, topologies in self.scenario_topology_protocol.items():
            for topology, protocols in topologies.items():
                for protocol, analysis_list in protocols.items():
                    if protocol_filter is not None and protocol not in protocol_filter:
                        continue

                    if scenario not in packets_stats_data:
                        packets_stats_data[scenario] = {}
                    if topology not in packets_stats_data[scenario]:
                        packets_stats_data[scenario][topology] = {'total': [0, 0]}
                    if protocol not in packets_stats_data[scenario][topology]:
                        packets_stats_data[scenario][topology][protocol] = []

                    for analysis_index, analysis_result in enumerate(analysis_list):
                        result = self.filter_packets(scenario, topology, protocol, analysis_index,
                                                     node_filter=node_filter,
                                                     packet_filter=packet_filter)

                        for node, stats in result.items():
                            if 'total' in node:
                                continue

                            for interface in stats:
                                if 'total' not in interface:
                                    continue

                                result[node] = result[node]['total']
                                packets_stats_data[scenario][topology]['total'][0] += result[node][0]
                                packets_stats_data[scenario][topology]['total'][1] += result[node][1]
                                break

                        packets_stats_data[scenario][topology][protocol].append(result)

        return packets_stats_data

    def filter_packets(self, scenario, topology, protocol, analysis_index, node_filter=None, packet_filter=None):
        result = MachineStatistic()

        packet_count = self.scenario_topology_protocol[scenario][topology][protocol][analysis_index]['packet_count']
        for node, node_packets_counter in packet_count.items():
            if 'total' in node or (node_filter is not None and not any(map(lambda x: x in node, node_filter))):
                continue

            for interface, interface_counter in node_packets_counter.items():
                if 'total' in interface:
                    continue

                for typology, (total_number, total_size) in interface_counter.items():
                    if packet_filter is None or typology in packet_filter:
                        if 'total' in typology:
                            continue

                        result.set_for_interface(node, interface, typology, total_number, total_size)

        return result.to_dict()

    @staticmethod
    def get_avg_values_for_experiment(packets_stats_data):
        logging.info("Computing average packet count/size statistics...")

        new_packets_stats_data = copy.deepcopy(packets_stats_data)

        for scenario, topologies in packets_stats_data.items():
            for topology, protocols in topologies.items():
                for protocol, analysis_list in protocols.items():
                    avg = {}

                    if 'total' in protocol:
                        del new_packets_stats_data[scenario][topology][protocol]
                        continue

                    for analysis_index, analysis_result in enumerate(analysis_list):
                        for node, (number, size) in analysis_result.items():
                            if node not in avg:
                                avg[node] = (0, 0)

                            (total_number, total_size) = avg[node]
                            new_total_number = total_number + number
                            new_total_size = total_size + size

                            if analysis_index == len(analysis_list) - 1:
                                avg[node] = (
                                    new_total_number / len(analysis_list), new_total_size / len(analysis_list)
                                )
                            else:
                                avg[node] = (new_total_number, new_total_size)

                    new_packets_stats_data[scenario][topology][protocol] = {'avg': avg}

        return new_packets_stats_data

    def get_packets_per_round_stats(self):
        logging.info("Computing round stats...")

        round_packets_stats = copy.deepcopy(self.scenario_topology_protocol)

        if 'Bootstrap' in round_packets_stats:
            del round_packets_stats['Bootstrap']
        if 'NormalOperation' in round_packets_stats:
            del round_packets_stats['NormalOperation']

        for scenario, topologies in self.scenario_topology_protocol.items():
            if 'Bootstrap' in scenario or 'NormalOperation' in scenario:
                continue

            for topology, protocols in topologies.items():
                for protocol, analysis_list in protocols.items():
                    for analysis_index, analysis_result in enumerate(analysis_list):
                        if 'packets_per_round' not in analysis_result:
                            print('Packets per round not computed for [%s, %s, %s, %d] ...' % (scenario, topology,
                                                                                               protocol,
                                                                                               analysis_index))
                            continue

                        round_packets_stats[scenario][topology][protocol][analysis_index] = \
                            analysis_result['packets_per_round']

        return round_packets_stats

    def get_avg_topological_distance_stats(self):
        logging.info("Computing average topological distance stats...")

        stats = self.get_topological_distance_stats()

        for scenario, values in stats.items():
            for topology_params, protocol_results in values.items():
                for protocol, results in protocol_results.items():
                    if len(results) > 1:
                        avg_results = SortedDict({str(i): [] for i in range(0, 4)})
                        for result in results:
                            for distance, packets in result.items():
                                avg_results[distance].append(packets)

                        for distance, packets in avg_results.items():
                            avg_results[distance] = mean(packets)

                        stats[scenario][topology_params][protocol] = avg_results
                    else:
                        stats[scenario][topology_params][protocol] = results.pop(0)

        return stats

    def get_topological_distance_stats(self):
        logging.info("Computing topological distance stats...")

        topological_distance_stats = copy.deepcopy(self.scenario_topology_protocol)

        if 'Bootstrap' in topological_distance_stats:
            del topological_distance_stats['Bootstrap']
        if 'NormalOperation' in topological_distance_stats:
            del topological_distance_stats['NormalOperation']

        for scenario, topologies in self.scenario_topology_protocol.items():
            if 'Bootstrap' in scenario or 'NormalOperation' in scenario:
                continue

            for topology, protocols in topologies.items():
                for protocol, analysis_list in protocols.items():
                    for analysis_index, analysis_result in enumerate(analysis_list):
                        if 'blast_radius' not in analysis_result:
                            print('Blast Radius not computed for [%s, %s, %s, %d] ...' % (scenario, topology,
                                                                                          protocol, analysis_index))
                            continue

                        for i in range(0, 4):
                            if str(i) not in analysis_result['blast_radius']['by_distance']:
                                analysis_result['blast_radius']['by_distance'][str(i)] = 0

                        topological_distance_stats[scenario][topology][protocol][analysis_index] = \
                            SortedDict(analysis_result['blast_radius']['by_distance'])

        return topological_distance_stats

    def get_avg_max_rounds_stats(self):
        logging.info("Computing average max rounds stats...")

        stats = self.get_max_rounds_stats()

        for scenario, values in stats.items():
            for topology_params, protocol_results in values.items():
                for protocol, results in protocol_results.items():
                    if len(results) > 1:
                        stats[scenario][topology_params][protocol] = mean(results)
                    else:
                        stats[scenario][topology_params][protocol] = results.pop(0)

        return stats

    def get_max_rounds_stats(self):
        logging.info("Computing max rounds stats...")

        max_number_of_rounds_stats = copy.deepcopy(self.scenario_topology_protocol)

        if 'Bootstrap' in max_number_of_rounds_stats:
            del max_number_of_rounds_stats['Bootstrap']
        if 'NormalOperation' in max_number_of_rounds_stats:
            del max_number_of_rounds_stats['NormalOperation']

        for scenario, topologies in self.scenario_topology_protocol.items():
            if 'Bootstrap' in scenario or 'NormalOperation' in scenario:
                continue

            for topology, protocols in topologies.items():
                for protocol, analysis_list in protocols.items():
                    for analysis_index, analysis_result in enumerate(analysis_list):
                        if 'max_number_of_rounds' not in analysis_result:
                            print('Max N Rounds not computed for [%s, %s, %s, %d] ...' % (scenario, topology,
                                                                                          protocol, analysis_index))
                            continue

                        max_number_of_rounds_stats[scenario][topology][protocol][analysis_index] = \
                            analysis_result['max_number_of_rounds']

        return max_number_of_rounds_stats

    def get_avg_max_scalar_clock_stats(self):
        logging.info("Computing average max logical clock stats...")

        stats = self.get_max_scalar_clock_stats()

        for scenario, values in stats.items():
            for topology_params, protocol_results in values.items():
                for protocol, results in protocol_results.items():
                    if len(results) > 1:
                        stats[scenario][topology_params][protocol] = mean(results)
                    else:
                        stats[scenario][topology_params][protocol] = results.pop(0)

        return stats

    def get_max_scalar_clock_stats(self):
        logging.info("Computing max logical clock stats...")

        max_number_of_scalar_clock_stats = copy.deepcopy(self.scenario_topology_protocol)

        if 'Bootstrap' in max_number_of_scalar_clock_stats:
            del max_number_of_scalar_clock_stats['Bootstrap']
        if 'NormalOperation' in max_number_of_scalar_clock_stats:
            del max_number_of_scalar_clock_stats['NormalOperation']

        for scenario, topologies in self.scenario_topology_protocol.items():
            if 'Bootstrap' in scenario or 'NormalOperation' in scenario:
                continue

            for topology, protocols in topologies.items():
                for protocol, analysis_list in protocols.items():
                    for analysis_index, analysis_result in enumerate(analysis_list):
                        if 'max_scalar_clock' not in analysis_result:
                            print('Scalar Clock not computed for [%s, %s, %s, %d] ...' % (scenario, topology,
                                                                                          protocol, analysis_index))
                            continue

                        max_number_of_scalar_clock_stats[scenario][topology][protocol][analysis_index] = \
                            analysis_result['max_scalar_clock']

        return max_number_of_scalar_clock_stats
