class GenericStatistic(object):
    __slots__ = ['counter']

    def __init__(self):
        self.counter = {}

    def update_by_key(self, key, value):
        if key not in self.counter:
            self.counter[key] = 0

        self.counter[key] += value

    def update(self, other):
        for key, value in other.to_dict().items():
            self.update_by_key(key, value)

    def to_dict(self):
        return self.counter
