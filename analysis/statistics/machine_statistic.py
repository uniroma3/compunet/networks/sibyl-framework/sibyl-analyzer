class MachineStatistic(object):
    __slots__ = ['counter']

    def __init__(self):
        self.counter = {}

    def update_for_interface(self, machine_name, interface, packet_type, packet_value=1, packet_size=0):
        if machine_name not in self.counter:
            self.counter[machine_name] = {'total': (0, 0)}

        if interface not in self.counter[machine_name]:
            self.counter[machine_name][interface] = {'total': (0, 0)}

        if packet_type not in self.counter[machine_name][interface]:
            self.counter[machine_name][interface][packet_type] = (0, 0)

        self.counter[machine_name][interface][packet_type] = self._update_tuple(
            self.counter[machine_name][interface][packet_type], packet_value, packet_size
        )

        self.counter[machine_name][interface]['total'] = self._update_tuple(
            self.counter[machine_name][interface]['total'], packet_value, packet_size
        )

        self.counter[machine_name]['total'] = self._update_tuple(
            self.counter[machine_name]['total'], packet_value, packet_size
        )

    def set_for_interface(self, machine_name, interface, packet_type, packet_value, packet_size):
        if machine_name not in self.counter:
            self.counter[machine_name] = {'total': (0, 0)}

        if interface not in self.counter[machine_name]:
            self.counter[machine_name][interface] = {'total': (0, 0)}

        if packet_type not in self.counter[machine_name][interface]:
            self.counter[machine_name][interface][packet_type] = (0, 0)

        self.counter[machine_name]['total'] = self._update_tuple(
            self.counter[machine_name]['total'],
            -self.counter[machine_name][interface][packet_type][0],
            -self.counter[machine_name][interface][packet_type][1]
        )

        self.counter[machine_name][interface]['total'] = self._update_tuple(
            self.counter[machine_name][interface]['total'],
            -self.counter[machine_name][interface][packet_type][0],
            -self.counter[machine_name][interface][packet_type][1]
        )

        self.counter[machine_name][interface][packet_type] = (packet_value, packet_size)

        self.counter[machine_name][interface]['total'] = self._update_tuple(
            self.counter[machine_name][interface]['total'], packet_value, packet_size
        )

        self.counter[machine_name]['total'] = self._update_tuple(
            self.counter[machine_name]['total'], packet_value, packet_size
        )

    def update(self, other):
        self.counter.update(other.counter)

    def to_dict(self):
        final_dict = {'total': (
            sum([self.counter[machine_name]['total'][0] for machine_name in self.counter.keys()]),
            sum([self.counter[machine_name]['total'][1] for machine_name in self.counter.keys()])
        )
        }
        final_dict.update(self.counter)

        return final_dict

    @staticmethod
    def _update_tuple(t, packet_value, packet_size):
        # 0 = Packet Count
        # 1 = Packet Size Total
        return t[0] + packet_value, t[1] + packet_size
