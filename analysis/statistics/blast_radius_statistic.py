from .generic_statistic import GenericStatistic


class BlastRadiusStatistic(object):
    __slots__ = ['by_cd_counter', 'by_distance_counter']

    def __init__(self):
        self.by_cd_counter = {}
        self.by_distance_counter = GenericStatistic()

    def update_by_cd_and_distance(self, collision_domain, distance, value):
        self.by_cd_counter[collision_domain] = (distance, value)
        self.by_distance_counter.update_by_key(distance, value)

    def to_dict(self):
        return {
            'by_collision_domain': self.by_cd_counter,
            'by_distance': self.by_distance_counter.to_dict()
        }
