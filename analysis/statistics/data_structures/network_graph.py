from math import inf

from .graph import Graph


class NetworkGraph(Graph):
    __slots__ = ['fat_tree_information']

    def __init__(self, fat_tree_information):
        super().__init__()

        self.fat_tree_information = fat_tree_information

        self._create_graph()

    def _create_graph(self):
        for machine_name, info in self.fat_tree_information:
            for neighbour_name, cd in info['neighbours']:
                self.add_edge(machine_name, neighbour_name, weight=0,
                              label=(cd, self._get_interface_number(info, cd)))

    def compute_distances_from_vertex(self, start):
        visited, queue = set(), self.vertices.copy()

        distances = {vertex: inf for vertex in self.vertices}
        distances[start] = 0
        previous_vertices = {vertex: None for vertex in self.vertices}
        edge_costs = {cd: 0 for (_, (_, (cd, _))) in self.get_vertex_neighbours(start, True)}
        while queue:
            # Select the unvisited node with the smallest distance.
            current_vertex = min(queue, key=lambda vertex: distances[vertex])

            # Stop, if the smallest distance among the unvisited nodes is infinity.
            if distances[current_vertex] == inf:
                break

            # Find unvisited neighbors for the current node and calculate their distances through the current node.
            for neighbour, (_, (cd, _)) in self.get_vertex_neighbours(current_vertex, True):
                alternative_route = distances[current_vertex] + 1

                # Compare the newly calculated distance to the assigned and save the smaller one.
                if alternative_route < distances[neighbour]:
                    distances[neighbour] = alternative_route
                    previous_vertices[neighbour] = current_vertex
                edge_costs[cd] = distances[neighbour]

            # Mark the current node as visited removing it from the unvisited set.
            queue.remove(current_vertex)

        return edge_costs

    @staticmethod
    def _get_interface_number(node_info, cd):
        for interface in node_info['interfaces']:
            if interface['collision_domain'] == cd:
                return interface['number']

        raise IndexError
