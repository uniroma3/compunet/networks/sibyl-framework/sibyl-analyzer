from collections import deque
from math import inf


class Graph(object):
    __slots__ = ['vertices', 'edges', 'edge_labels']

    def __init__(self):
        self.vertices = set()
        self.edges = {}
        self.edge_labels = {}

    def add_vertex(self, vertex_name):
        if vertex_name not in self.vertices:
            self.vertices.add(vertex_name)

    def add_edge(self, start, end, weight=None, label=None):
        self.add_vertex(start)
        self.add_vertex(end)

        if start not in self.edges:
            self.edges[start] = {}

        if end not in self.edges[start]:
            self.edges[start][end] = 0 if weight is not None else None

        if weight is not None:
            if self.edges[start][end]:
                self.edges[start][end] += weight
            else:
                self.edges[start][end] = weight

        if label:
            self.add_label(start, end, label)

    def add_label(self, start, end, label):
        if start not in self.vertices:
            raise ValueError

        if start not in self.edge_labels:
            self.edge_labels[start] = {}

        self.edge_labels[start][end] = label

    def get_vertex_neighbours(self, vertex_name, with_label=False):
        if vertex_name not in self.edges:
            return []

        neighbours = self.edges[vertex_name].copy()

        if with_label and vertex_name in self.edge_labels:
            for neighbour, label in self.edge_labels[vertex_name].items():
                neighbours[neighbour] = (neighbours[neighbour], label)

        return list(neighbours.items())

    def get_vertex_edges(self, vertex_name):
        return self.get_vertex_inbound_edges(vertex_name), self.get_vertex_outbound_edges(vertex_name)

    def get_vertex_outbound_edges(self, vertex_name):
        outbound_edges = []
        if vertex_name not in self.edges:
            return outbound_edges

        for neighbour, weight in self.edges[vertex_name].items():
            outbound_edges.append((vertex_name, neighbour, weight))

        return outbound_edges

    def get_vertex_inbound_edges(self, vertex_name):
        inbound_edges = []
        for vertex in self.vertices:
            if vertex_name == vertex or vertex not in self.edges:
                continue
            if vertex_name in self.edges[vertex]:
                inbound_edges.append((vertex, vertex_name, self.edges[vertex][vertex_name]))

        return inbound_edges

    def get_all_paths_by_source_to_dest(self, source, destination):
        visited = {}
        path = []

        return self.get_all_paths_by_source_to_dest_aux(source, destination, visited, path)

    def get_all_paths_by_source_to_dest_aux(self, source, destination, visited, path):
        paths = []

        visited[source] = True
        path.append(source)

        if source == destination:
            paths.append(path.copy())
        else:
            for neighbour, _ in self.get_vertex_neighbours(source):
                if neighbour not in visited or not visited[neighbour]:
                    paths.extend(self.get_all_paths_by_source_to_dest_aux(neighbour, destination, visited, path))

        path.pop()
        visited[source] = False

        return paths

    def compute_maximum_distances(self, start):
        distances = {vertex: -inf for vertex in self.vertices}
        distances[start] = 0
        previous_vertices = {vertex: None for vertex in self.vertices}

        queue = self.vertices.copy()

        while queue:
            # Select the unvisited node with the maximum distance.
            current_vertex = max(queue, key=lambda vertex: distances[vertex])

            # Stop, if the maximum distance among the unvisited nodes is -infinity.
            if distances[current_vertex] == -inf:
                break

            # Find unvisited neighbors for the current node
            # and calculate their distances through the current node.
            for neighbour, _ in self.get_vertex_neighbours(current_vertex):
                cost_to_neighbour = distances[current_vertex] + 1

                # Compare the newly calculated distance to the assigned and save the bigger one.
                if cost_to_neighbour > distances[neighbour]:
                    distances[neighbour] = cost_to_neighbour
                    previous_vertices[neighbour] = current_vertex
                    self._update_distance_of_neighbours_of(neighbour, previous_vertices, distances)

            # Mark the current node as visited removing it from the unvisited set.
            queue.remove(current_vertex)

        return distances

    def _update_distance_of_neighbours_of(self, vertex, previous_vertices, distances):
        queue = deque(map(lambda x: (vertex, x[0]), self.get_vertex_neighbours(vertex)))

        while queue:
            (v, neighbour) = queue.popleft()

            if distances[v] > distances[neighbour]:
                distances[neighbour] = distances[v] + 1
                previous_vertices[neighbour] = v
                queue.extendleft(map(lambda x: (neighbour, x[0]),
                                     self.get_vertex_neighbours(neighbour)))

    def edge_exists(self, source, dest):
        if source not in self.edges:
            return False

        return dest in self.edges[source]

    def to_d3(self):
        d3_format = {
            "nodes": list(map(lambda x: {"id": x, "label": x}, self.vertices)),
            "links": []
        }

        for vertex, neighbours in self.edges.items():
            for neighbour, weight in neighbours.items():
                link_item = {
                    "source": vertex,
                    "target": neighbour,
                    "value": weight,
                }

                if vertex in self.edge_labels and neighbour in self.edge_labels[vertex]:
                    link_item['label'] = self.edge_labels[vertex][neighbour]

                d3_format["links"].append(link_item)

        return d3_format

    @staticmethod
    def from_d3(d3_format):
        graph = Graph()

        for link in d3_format['links']:
            graph.add_edge(link['source'], link['target'], weight=link['value'])

        return graph

    def __repr__(self):
        return "Vertices: %s\n" \
               "Edges: %s\n" \
               "Edges Labels: %s" % (str(self.vertices), str(self.edges), str(self.edge_labels))
