from analysis.statistics.data_structures.graph import Graph
from ..statistics.blast_radius_statistic import BlastRadiusStatistic


class BlastRadiusAnalyzer(object):
    __slots__ = ['network_graph', 'collision_domain_stats', 'stats', 'radius_graph']

    def __init__(self, network_graph, cd_packet_stats):
        self.network_graph = network_graph
        self.collision_domain_stats = cd_packet_stats

        self.stats = BlastRadiusStatistic()
        self.radius_graph = Graph()

    def analyze(self, starting_node):
        cd_distances_from_node = self.network_graph.compute_distances_from_vertex(starting_node)

        for cd_name, distance in cd_distances_from_node.items():
            if cd_name in self.collision_domain_stats:
                self.stats.update_by_cd_and_distance(cd_name, distance,
                                                     self.collision_domain_stats[cd_name])

        self._compute_blast_radius_graph()

    def _compute_blast_radius_graph(self):
        for vertex, neighbours in self.network_graph.edges.items():
            if 'server' in vertex:
                continue

            for neighbour in neighbours:
                if 'server' in neighbour:
                    continue

                (cd, _) = self.network_graph.edge_labels[vertex][neighbour]
                weight = self.stats.by_cd_counter[cd][1] if cd in self.stats.by_cd_counter else 0

                self.radius_graph.add_edge(vertex, neighbour, weight=weight, label=cd)
