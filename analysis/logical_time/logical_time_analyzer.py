from sortedcontainers import SortedDict


class LogicalTimeAnalyzer(object):
    __slots__ = ["labelling_callback", "points_by_machine", "packet_edges", "max_scalar_clock"]

    def __init__(self):
        self.labelling_callback = None

        self.points_by_machine = {}
        self.packet_edges = []

        self.max_scalar_clock = -1

    @property
    def logical_time_plot(self):
        return {
            'points': self.points_by_machine,
            'edges': self.packet_edges
        }

    def analyze(self, groups, packets_by_machine, labelling_callback):
        self.labelling_callback = labelling_callback

        packets_by_machine = {key: dict(list(enumerate(value))) for key, value in packets_by_machine.items()}

        logical_time_by_machine = {}

        for group in groups:
            for (source, dest, packet) in group:
                if source not in logical_time_by_machine:
                    logical_time_by_machine[source] = 0

                if dest not in logical_time_by_machine:
                    logical_time_by_machine[dest] = 0

                logical_time_by_machine[source] += 1
                logical_time_by_machine[dest] = max(logical_time_by_machine[dest], logical_time_by_machine[source]) + 1

                self.max_scalar_clock = max(self.max_scalar_clock, logical_time_by_machine[source],
                                            logical_time_by_machine[dest])

                dest_packet = self._get_packet(packets_by_machine, source, dest, packet)

                self._add_packet_to_points(source, logical_time_by_machine[source], packet)

                dest_idx = None
                if dest_packet is not None:
                    self._add_packet_to_points(dest, logical_time_by_machine[dest], dest_packet)
                    dest_idx = logical_time_by_machine[dest]

                self.packet_edges.append((
                    (source, logical_time_by_machine[source]),
                    (dest, dest_idx)
                ))

    def _add_packet_to_points(self, machine_name, idx, packet):
        if machine_name not in self.points_by_machine:
            self.points_by_machine[machine_name] = SortedDict()

        self.points_by_machine[machine_name][idx] = self.labelling_callback(packet, clock=idx)

    @staticmethod
    def _get_packet(packets_by_machine, source_machine_name, dest_machine_name, packet):
        new_machine_packs = packets_by_machine[dest_machine_name].copy()
        found_packet = None

        for idx, (source, dest, p) in packets_by_machine[dest_machine_name].items():
            if source_machine_name == source and dest_machine_name == dest and bytes(packet) == bytes(p):
                found_packet = p
                del new_machine_packs[idx]
                break

        if found_packet is not None:
            packets_by_machine[dest_machine_name] = new_machine_packs

        return found_packet
