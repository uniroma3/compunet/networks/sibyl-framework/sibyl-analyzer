import ipaddress
import math
import struct

from .packet import Packet


class Bgp(Packet):
    __slots__ = ['length', 'type', 'marker', 'withdrawn', 'announced']

    def __init__(self, payload):
        super().__init__(payload)

        self._parse_marker(payload)
        self._parse_hdr_length(payload)
        self._parse_type(payload)

        if self.type == 2:
            self.withdrawn = []
            self.announced = []
            withdrawn_length = self._parse_withdrawn()
            path_attribute_length = struct.unpack('>H',
                                                  self.payload[
                                                  21 + int(withdrawn_length):21 + int(withdrawn_length) + 2])[0]
            self._parse_announced(withdrawn_length, path_attribute_length)

    def _parse_marker(self, payload):
        self.marker = payload[:16]
        if self.marker != b'\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff':
            raise Exception("BGP Message Marker Malformed", self.marker)

    def _parse_hdr_length(self, payload):
        self.length = struct.unpack('>H', payload[16:18])[0]
        self.payload = payload[:self.length]
        if self.length != len(self.payload):
            raise Exception("BGP Message Hdr Length Malformed", self.length, len(payload))

    def _parse_type(self, payload):
        self.type = struct.unpack('>B', payload[18:19])[0]
        if self.type not in [1, 2, 3, 4, 5]:
            raise Exception("BGP Message Type Malformed")

    def _parse(self, length):
        parsed = self.payload[:length]
        self.payload = self.payload[length:]
        return parsed

    def _parse_routes(self, routes_bytes, routes_list):
        while len(routes_bytes) > 0:
            mask = routes_bytes[0]
            routes_bytes = routes_bytes[1:]
            if mask > 0:
                byte_length = math.ceil(mask / 8)
                prefix = routes_bytes[:byte_length]
                prefix += (b'\x00' * (4 - byte_length))
                try:
                    route = '%s/%s' % (ipaddress.ip_address(prefix), mask)
                    routes_list.append(route)
                    routes_bytes = routes_bytes[byte_length:]
                except:
                    raise Exception('Route Malformed')

    def _parse_withdrawn(self):
        withdrawn_length = struct.unpack('>H', self.payload[19:21])[0]
        withdrawn = self.payload[21:21 + int(withdrawn_length)]
        self._parse_routes(withdrawn, self.withdrawn)
        return withdrawn_length

    def _parse_announced(self, withdrawn_length, path_attribute_length):
        updates = self.payload[21 + int(withdrawn_length) + 2 + path_attribute_length:]
        self._parse_routes(updates, self.announced)
