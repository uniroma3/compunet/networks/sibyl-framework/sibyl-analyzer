class Packet(object):
    __slots__ = ['payload', 'src', 'time', '_len']

    def __init__(self, payload):
        self.payload = payload

        self.src = None
        self.time = 0

        self._len = None

    def __len__(self):
        if self._len is None:
            self._len = len(self.payload)

        return self._len

    def __bytes__(self):
        return self.payload
