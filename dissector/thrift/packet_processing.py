# This code is took from python-rift and has been adapted/cut to work as dissector

import ipaddress

import thrift.protocol.TBinaryProtocol
import thrift.transport.TTransport

from . import constants
from .common import ttypes as common_ttypes
from .encoding import constants as encoding_constants
from .encoding import ttypes as encoding_ttypes


def encode_packet(pkt):
    fix_protocol_packet_before_encode(pkt)

    transport_out = thrift.transport.TTransport.TMemoryBuffer()
    protocol_out = thrift.protocol.TBinaryProtocol.TBinaryProtocol(transport_out)

    pkt.write(protocol_out)

    return transport_out.getvalue()


def decode_packet(pkt):
    transport_in = thrift.transport.TTransport.TMemoryBuffer(pkt)
    protocol_in = thrift.protocol.TBinaryProtocol.TBinaryProtocol(transport_in)
    protocol_packet = encoding_ttypes.ProtocolPacket()

    # noinspection PyBroadException
    try:
        protocol_packet.read(protocol_in)
    # We don't know what exception Thrift might throw
    except Exception:
        return None

    try:
        protocol_packet.validate()
    except thrift.protocol.TProtocol.TProtocolException as err:
        return None

    fix_protocol_packet_after_decode(protocol_packet)

    return protocol_packet


def ipv4_prefix_tup(ipv4_prefix):
    return ipv4_prefix.address, ipv4_prefix.prefixlen


def ipv6_prefix_tup(ipv6_prefix):
    return ipv6_prefix.address, ipv6_prefix.prefixlen


def ip_prefix_tup(ip_prefix):
    assert (ip_prefix.ipv4prefix is None) or (ip_prefix.ipv6prefix is None)
    assert (ip_prefix.ipv4prefix is not None) or (ip_prefix.ipv6prefix is not None)

    if ip_prefix.ipv4prefix:
        return 4, ipv4_prefix_tup(ip_prefix.ipv4prefix)

    return 6, ipv6_prefix_tup(ip_prefix.ipv6prefix)


def tie_id_tup(tie_id):
    return tie_id.direction, tie_id.originator, tie_id.tietype, tie_id.tie_nr


def tie_header_tup(tie_header):
    return (tie_header.tieid, tie_header.seq_nr,
            tie_header.origination_time)


def link_id_pair_tup(link_id_pair):
    return link_id_pair.local_id, link_id_pair.remote_id


def timestamp_tup(timestamp):
    return timestamp.AS_sec, timestamp.AS_nsec


def add_missing_methods_to_thrift():
    # See http://bit.ly/thrift-missing-hash for details about why this is needed
    common_ttypes.IPv4PrefixType.__hash__ = (
        lambda self: hash(ipv4_prefix_tup(self)))
    common_ttypes.IPv4PrefixType.__eq__ = (
        lambda self, other: ipv4_prefix_tup(self) == ipv4_prefix_tup(other))
    common_ttypes.IPv6PrefixType.__hash__ = (
        lambda self: hash(ipv6_prefix_tup(self)))
    common_ttypes.IPv6PrefixType.__eq__ = (
        lambda self, other: ipv6_prefix_tup(self) == ipv6_prefix_tup(other))
    common_ttypes.IPPrefixType.__hash__ = (
        lambda self: hash(ip_prefix_tup(self)))
    common_ttypes.IPPrefixType.__eq__ = (
        lambda self, other: ip_prefix_tup(self) == ip_prefix_tup(other))
    common_ttypes.IPPrefixType.__str__ = ip_prefix_str
    common_ttypes.IPPrefixType.__lt__ = (
        lambda self, other: ip_prefix_tup(self) < ip_prefix_tup(other))
    common_ttypes.IEEE802_1ASTimeStampType.__hash__ = (
        lambda self: hash(timestamp_tup(self)))
    common_ttypes.IEEE802_1ASTimeStampType.__eq__ = (
        lambda self, other: timestamp_tup(self) == timestamp_tup(other))
    encoding_ttypes.TIEID.__hash__ = (
        lambda self: hash(tie_id_tup(self)))
    encoding_ttypes.TIEID.__eq__ = (
        lambda self, other: tie_id_tup(self) == tie_id_tup(other))
    encoding_ttypes.TIEID.__lt__ = (
        lambda self, other: tie_id_tup(self) < tie_id_tup(other))
    encoding_ttypes.TIEHeader.__hash__ = (
        lambda self: hash(tie_header_tup(self)))
    encoding_ttypes.TIEHeader.__eq__ = (
        lambda self, other: tie_header_tup(self) == tie_header_tup(other))
    encoding_ttypes.TIEHeaderWithLifeTime.__hash__ = (
        lambda self: hash((tie_header_tup(self.header), self.remaining_lifetime)))
    encoding_ttypes.TIEHeaderWithLifeTime.__eq__ = (
        lambda self, other: (tie_header_tup(self.header) == tie_header_tup(other.header)) and
                            self.remaining_lifetime == other.remaining_lifetime)
    encoding_ttypes.LinkIDPair.__hash__ = (
        lambda self: hash(link_id_pair_tup(self)))
    encoding_ttypes.LinkIDPair.__eq__ = (
        lambda self, other: link_id_pair_tup(self) == link_id_pair_tup(other))
    encoding_ttypes.LinkIDPair.__hash__ = (
        lambda self: hash(link_id_pair_tup(self)))
    encoding_ttypes.LinkIDPair.__lt__ = (
        lambda self, other: link_id_pair_tup(self) < link_id_pair_tup(other))


# What follows are some horrible hacks to deal with the fact that Thrift only support signed 8, 16,
# 32, and 64 bit numbers and not unsigned 8, 16, 32, and 64 bit numbers. The RIFT specification has
# several fields are intended to contain an unsigned numbers, but that are actually specified in the
# .thrift files as a signed numbers. Just look for the following text in the specification: "MUST be
# interpreted in implementation as unsigned ..." where ... can be 8 bits, or 16 bits, or 32 bits, or
# 64 bits. Keep in mind Python does not have sized integers: values of type int are unbounded (i.e.
# they have no limit on the size and no minimum or maximum value).

MAX_U64 = 0xffffffffffffffff
MAX_S64 = 0x7fffffffffffffff

MAX_U32 = 0xffffffff
MAX_S32 = 0x7fffffff

MAX_U16 = 0xffff
MAX_S16 = 0x7fff

MAX_U8 = 0xff
MAX_S8 = 0x7f


def u64_to_s64(u64):
    return u64 if u64 <= MAX_S64 else u64 - MAX_U64 - 1


def u32_to_s32(u32):
    return u32 if u32 <= MAX_S32 else u32 - MAX_U32 - 1


def u16_to_s16(u16):
    return u16 if u16 <= MAX_S16 else u16 - MAX_U16 - 1


def u8_to_s8(u08):
    return u08 if u08 <= MAX_S8 else u08 - MAX_U8 - 1


def s64_to_u64(s64):
    return s64 if s64 >= 0 else s64 + MAX_U64 + 1


def s32_to_u32(s32):
    return s32 if s32 >= 0 else s32 + MAX_U32 + 1


def s16_to_u16(s16):
    return s16 if s16 >= 0 else s16 + MAX_U16 + 1


def s8_to_u8(s08):
    return s08 if s08 >= 0 else s08 + MAX_U8 + 1


def fix_int(value, size, encode):
    if encode:
        # Fix before encode
        if size == 8:
            return u8_to_s8(value)
        if size == 16:
            return u16_to_s16(value)
        if size == 32:
            return u32_to_s32(value)
        if size == 64:
            return u64_to_s64(value)
        assert False
    else:
        # Fix after decode
        if size == 8:
            return s8_to_u8(value)
        if size == 16:
            return s16_to_u16(value)
        if size == 32:
            return s32_to_u32(value)
        if size == 64:
            return s64_to_u64(value)
        assert False

    return value  # Unreachable, stop pylint from complaining about inconsistent-return-statements


def fix_dict(old_dict, dict_fixes, encode):
    (key_fixes, value_fixes) = dict_fixes
    new_dict = {}
    for the_key, value in old_dict.items():
        new_key = fix_value(the_key, key_fixes, encode)
        new_value = fix_value(value, value_fixes, encode)
        new_dict[new_key] = new_value
    return new_dict


def fix_struct(fixed_struct, fixes, encode):
    for fix in fixes:
        (field_name, field_fix) = fix
        if field_name in vars(fixed_struct):
            field_value = getattr(fixed_struct, field_name)
            if field_value is not None:
                new_value = fix_value(field_value, field_fix, encode)
                setattr(fixed_struct, field_name, new_value)
    return fixed_struct


def fix_set(old_set, fix, encode):
    new_set = set()
    for old_value in old_set:
        new_value = fix_value(old_value, fix, encode)
        new_set.add(new_value)
    return new_set


def fix_list(old_list, fix, encode):
    new_list = []
    for old_value in old_list:
        new_value = fix_value(old_value, fix, encode)
        new_list.append(new_value)
    return new_list


def fix_value(value, fix, encode):
    if isinstance(value, set):
        new_value = fix_set(value, fix, encode)
    elif isinstance(value, list):
        new_value = fix_list(value, fix, encode)
    elif isinstance(fix, int):
        new_value = fix_int(value, fix, encode)
    elif isinstance(fix, tuple):
        new_value = fix_dict(value, fix, encode)
    elif isinstance(fix, list):
        new_value = fix_struct(value, fix, encode)
    else:
        assert False
    return new_value


def fix_packet_before_encode(packet, fixes):
    fix_struct(packet, fixes, True)


def fix_packet_after_decode(packet, fixes):
    fix_struct(packet, fixes, False)


TIEID_FIXES = [
    ('originator', 64), ('tie_nr', 32)
]

TIMESTAMP_FIXES = [
    ('AS_sec', 64), ('AS_nsec', 32)
]

TIE_HEADER_FIXES = [
    ('tieid', TIEID_FIXES), ('seq_nr', 32),
    ('origination_time', TIMESTAMP_FIXES),
    ('origination_lifetime', 32)
]

TIE_HEADER_WITH_LIFETIME_FIXES = [
    ('header', TIE_HEADER_FIXES), ('remaining_lifetime', 32),
]

LINK_ID_PAIR_FIXES = [
    ('local_id', 32),  # Draft doesn't mention this needs to treated as unsigned
    ('remote_id', 32)  # Draft doesn't mention this needs to treated as unsigned
]

NODE_NEIGHBORS_TIE_ELEMENT_FIXES = [
    ('level', 16), ('cost', 32), ('link_ids', LINK_ID_PAIR_FIXES), ('bandwidth', 32)
]

IP_PREFIX_FIXES = [
    ('ipv4prefix', [
        ('address', 32),
        ('prefixlen', 8)  # Draft doesn't mention this needs to treated as unsigned
    ]),
    ('ipv6prefix', [
        ('prefixlen', 8)  # Draft doesn't mention this needs to treated as unsigned
    ])
]

PREFIX_ATTRIBUTES_FIXES = [
    ('metric', 32), ('tags', 64),
    ('monotonic_clock', [
        ('timestamp', TIMESTAMP_FIXES), ('transactionid', 8)
    ])
]

PREFIX_TIE_ELEMENT_FIXES = [
    ('prefixes', (IP_PREFIX_FIXES, PREFIX_ATTRIBUTES_FIXES))
]

PROTOCOL_PACKET_FIXES = [
    ('header', [
        ('major_version', 16), ('minor_version', 16),
        ('sender', 64), ('level', 16)]),
    ('content', [
        ('lie', [
            ('local_id', 32),  # Draft doesn't mention this needs to treated as unsigned
            ('flood_port', 16), ('link_mtu_size', 32), ('link_bandwidth', 32),
            ('neighbor', [
                ('originator', 64),
                ('remote_id', 32)  # Draft doesn't mention this needs to treated as unsigned
            ]),
            ('pod', 32),
            ('holdtime', 16),  # Draft doesn't mention this needs to treated as unsigned
            ('label', 32)]),
        ('tide', [
            ('start_range', TIEID_FIXES),
            ('end_range', TIEID_FIXES),
            ('headers', TIE_HEADER_WITH_LIFETIME_FIXES)
        ]),
        ('tire', [
            ('headers', TIE_HEADER_WITH_LIFETIME_FIXES)
        ]),
        ('tie', [
            ('header', TIE_HEADER_FIXES),
            ('element', [
                ('node', [
                    ('level', 16),
                    ('neighbors', (64, NODE_NEIGHBORS_TIE_ELEMENT_FIXES))
                ]),
                ('prefixes', PREFIX_TIE_ELEMENT_FIXES),
                ('positive_disaggregation_prefixes', PREFIX_TIE_ELEMENT_FIXES),
                ('negative_disaggregation_prefixes', PREFIX_TIE_ELEMENT_FIXES),
                ('external_prefixes', PREFIX_TIE_ELEMENT_FIXES),
            ])
        ])
    ])
]


def fix_protocol_packet_before_encode(protocol_packet):
    fix_packet_before_encode(protocol_packet, PROTOCOL_PACKET_FIXES)


def fix_protocol_packet_after_decode(protocol_packet):
    fix_packet_after_decode(protocol_packet, PROTOCOL_PACKET_FIXES)


def make_tie_id(direction, originator, tie_type, tie_nr):
    tie_id = encoding_ttypes.TIEID(
        direction=direction,
        originator=originator,
        tietype=tie_type,
        tie_nr=tie_nr)
    return tie_id


def make_tie_header(direction, originator, tie_type, tie_nr, seq_nr,
                    origination_time=None):
    tie_id = make_tie_id(direction, originator, tie_type, tie_nr)
    tie_header = encoding_ttypes.TIEHeader(
        tieid=tie_id,
        seq_nr=seq_nr,
        origination_time=origination_time)
    return tie_header


def make_tie_header_with_lifetime(direction, originator,
                                  tie_type, tie_nr, seq_nr,
                                  lifetime,
                                  origination_time=None):
    tie_header_with_lifetime = encoding_ttypes.TIEHeaderWithLifeTime(
        header=make_tie_header(direction, originator, tie_type, tie_nr, seq_nr, origination_time),
        remaining_lifetime=lifetime)
    return tie_header_with_lifetime


def expand_tie_header_with_lifetime(tie_header, lifetime):
    return encoding_ttypes.TIEHeaderWithLifeTime(header=tie_header, remaining_lifetime=lifetime)


def make_prefix_tie_packet(direction, originator, tie_nr, seq_nr):
    tie_type = common_ttypes.TIETypeType.PrefixTIEType
    tie_header = make_tie_header(direction, originator, tie_type, tie_nr, seq_nr)
    prefixes = {}
    prefix_tie_element = encoding_ttypes.PrefixTIEElement(prefixes=prefixes)
    tie_element = encoding_ttypes.TIEElement(prefixes=prefix_tie_element)
    tie_packet = encoding_ttypes.TIEPacket(header=tie_header, element=tie_element)
    return tie_packet


def make_ip_address(address_str):
    if ":" in address_str:
        return make_ipv6_address(address_str)
    else:
        return make_ipv4_address(address_str)


def make_ipv4_address(address_str):
    return ipaddress.IPv4Address(address_str)


def make_ipv6_address(address_str):
    return ipaddress.IPv6Address(address_str)


def make_ip_prefix(prefix_str):
    if ":" in prefix_str:
        return make_ipv6_prefix(prefix_str)
    else:
        return make_ipv4_prefix(prefix_str)


def make_ipv4_prefix(prefix_str):
    ipv4_network = ipaddress.IPv4Network(prefix_str)
    address = int(ipv4_network.network_address)
    prefixlen = ipv4_network.prefixlen
    ipv4_prefix = common_ttypes.IPv4PrefixType(address, prefixlen)
    prefix = common_ttypes.IPPrefixType(ipv4prefix=ipv4_prefix)
    return prefix


def make_ipv6_prefix(prefix_str):
    ipv6_network = ipaddress.IPv6Network(prefix_str)
    address = ipv6_network.network_address.packed
    prefixlen = ipv6_network.prefixlen
    ipv6_prefix = common_ttypes.IPv6PrefixType(address, prefixlen)
    prefix = common_ttypes.IPPrefixType(ipv6prefix=ipv6_prefix)
    return prefix


def add_ipv4_prefix_to_prefix_tie(prefix_tie_packet, prefix, metric, tags=None,
                                  monotonic_clock=None):
    attributes = encoding_ttypes.PrefixAttributes(metric, tags, monotonic_clock)
    prefix_tie_packet.element.prefixes.prefixes[prefix] = attributes


def add_ipv6_prefix_to_prefix_tie(prefix_tie_packet, ipv6_prefix_string, metric, tags=None,
                                  monotonic_clock=None):
    prefix = make_ipv6_prefix(ipv6_prefix_string)
    attributes = encoding_ttypes.PrefixAttributes(metric=metric,
                                                  tags=tags,
                                                  monotonic_clock=monotonic_clock)
    prefix_tie_packet.element.prefixes.prefixes[prefix] = attributes


def make_node_tie_packet(name, level, direction, originator, tie_nr, seq_nr):
    tie_type = common_ttypes.TIETypeType.NodeTIEType
    tie_header = make_tie_header(direction, originator, tie_type, tie_nr, seq_nr)

    node_tie_element = encoding_ttypes.NodeTIEElement(
        level=level,
        neighbors={},
        capabilities=encoding_ttypes.NodeCapabilities(
            protocol_minor_version=encoding_constants.protocol_minor_version,
            flood_reduction=True,
        ),
        flags=None,  # TODO: Implement this
        name=name)
    tie_element = encoding_ttypes.TIEElement(node=node_tie_element)
    tie_packet = encoding_ttypes.TIEPacket(header=tie_header, element=tie_element)
    return tie_packet


def make_tide_packet(start_range, end_range):
    tide_packet = encoding_ttypes.TIDEPacket(start_range=start_range,
                                             end_range=end_range,
                                             headers=[])
    return tide_packet


def add_tie_header_to_tide(tide_packet, tie_header):
    assert tie_header.__class__ == encoding_ttypes.TIEHeaderWithLifeTime
    tide_packet.headers.append(tie_header)


def make_tire_packet():
    tire_packet = encoding_ttypes.TIREPacket(headers=set())
    return tire_packet


def add_tie_header_to_tire(tire_packet, tie_header):
    assert tie_header.__class__ == encoding_ttypes.TIEHeaderWithLifeTime
    tire_packet.headers.add(tie_header)


def ipv4_prefix_str(ipv4_prefix):
    address = ipv4_prefix.address
    length = ipv4_prefix.prefixlen
    return str(ipaddress.IPv4Network((address, length)))


def ipv6_prefix_str(ipv6_prefix):
    address = ipv6_prefix.address.rjust(16, b"\x00")
    length = ipv6_prefix.prefixlen
    return str(ipaddress.IPv6Network((address, length)))


def ip_prefix_str(ip_prefix):
    assert (ip_prefix.ipv4prefix is None) or (ip_prefix.ipv6prefix is None)
    assert (ip_prefix.ipv4prefix is not None) or (ip_prefix.ipv6prefix is not None)
    result = ""
    if ip_prefix.ipv4prefix:
        result += ipv4_prefix_str(ip_prefix.ipv4prefix)
    if ip_prefix.ipv6prefix:
        result += ipv6_prefix_str(ip_prefix.ipv6prefix)
    return result


def assert_prefix_address_family(prefix, address_family):
    assert isinstance(prefix, common_ttypes.IPPrefixType)
    if address_family == constants.ADDRESS_FAMILY_IPV4:
        assert prefix.ipv4prefix is not None
        assert prefix.ipv6prefix is None
    elif address_family == constants.ADDRESS_FAMILY_IPV6:
        assert prefix.ipv4prefix is None
        assert prefix.ipv6prefix is not None
    else:
        assert False
