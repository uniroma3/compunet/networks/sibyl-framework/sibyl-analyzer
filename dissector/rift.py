import struct

from .packet import Packet
from .thrift.packet_processing import decode_packet, add_missing_methods_to_thrift

add_missing_methods_to_thrift()

rift_types = {
    "LIE": 0,
    "TIDE": 1,
    "TIRE": 2,
    "TIE": 3
}


class Rift(Packet):
    __slots__ = ['type', 'serialized_object']

    def __init__(self, payload):
        super().__init__(payload)

        lifetime_offset = 12 + self.payload[7]
        remaining_tie_lifetime = struct.unpack(">L", self.payload[lifetime_offset:lifetime_offset + 4])[0]

        if remaining_tie_lifetime == 0xffffffff:
            content_offset = lifetime_offset + 4
            self.serialized_object = decode_packet(self.payload[content_offset:])

            if self.serialized_object.content.tide:
                self.type = rift_types['TIDE']
            elif self.serialized_object.content.tire:
                self.type = rift_types['TIRE']
            else:
                self.type = rift_types['LIE']
        else:
            self.type = rift_types['TIE']

            content_offset = lifetime_offset + 4 + 3
            origin_fingerprint_len = self.payload[content_offset]
            content_offset += origin_fingerprint_len + 1

            self.serialized_object = decode_packet(self.payload[content_offset:])
