import struct

from .packet import Packet

scopes = {
    1: "Level 1 Circuit Flooding Scope",
    2: "Level 2 Circuit Flooding Scope",
    3: "Level 1 Flooding Scope",
    4: "Level 2 Flooding Scope",
    5: "Domain Flooding Scope",
    64: "Level 1 Circuit Flooding Scope",
    65: "Level 2 Circuit Flooding Scope",
    66: "Level 1 Flooding Scope",
    67: "Level 2 Flooding Scope",
    68: "Domain Flooding Scope"
}


class Isis(Packet):
    __slots__ = ['type', '_lspid', '_seqnum', '_dynamic_hostname', '_scope', '_priority']

    def __init__(self, payload):
        super().__init__(payload)

        self.type = int(self.payload[4])

        self._lspid = None
        self._seqnum = None
        self._dynamic_hostname = None
        self._scope = None
        self._priority = None

    @property
    def lspid(self):
        if self._lspid is None and self.type in [10, 18, 20]:
            self._lspid = (("%02X%02X." * 3) + "%02X-%02X") % (tuple(x for x in self.payload[12:20]))

        return self._lspid

    @property
    def seqnum(self):
        if self._seqnum is None and self.type in [10, 18, 20]:
            self._seqnum = struct.unpack(">L", self.payload[20:24])[0]

        return self._seqnum

    @property
    def dynamic_hostname(self):
        if self._dynamic_hostname is None:
            tlvs = self.payload[30:]
            while tlvs:
                if tlvs[0] != 0x89:
                    tlvs = tlvs[tlvs[1] + 2:]
                else:
                    tlvs = tlvs[1:]
                    dynamic_hostname_len = tlvs[0] + 1

                    self._dynamic_hostname = tlvs[1:dynamic_hostname_len].decode('utf-8')
                    break

        return self._dynamic_hostname

    @property
    def scope(self):
        if self._scope is None and self.type == 10:
            scope = self.payload[7] & 0x7f
            self._scope = scopes[scope] if scope in scopes else "Unassigned"

        return self._scope

    @property
    def priority(self):
        if self._priority is None and self.type == 10:
            self._priority = (self.payload[7] & 0x80) == 0x80

        return self._priority
