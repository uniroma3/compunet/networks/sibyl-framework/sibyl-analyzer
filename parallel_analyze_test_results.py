#!/usr/bin/python3

import logging
import os
import shlex
import shutil
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool
from pathlib import Path
from subprocess import Popen, DEVNULL

import coloredlogs

from utils import parse_analysis_arguments

if __name__ == '__main__':
    coloredlogs.install(fmt='[%(asctime)s] %(levelname)s - %(message)s', level="INFO")
    args = parse_analysis_arguments()

    current_file_path = os.path.dirname(Path(__file__).absolute())

    if not args.selected_path:
        results_directory = os.path.abspath(args.base_path)

        results_dirs = sorted(filter(lambda x: "_results" in x, os.listdir(results_directory)))
    else:
        results_dirs = [args.selected_path]
        results_directory = ""

    executable_path = os.path.join(current_file_path, "analyze_test_results.py")
    logs_dir = os.path.join(current_file_path, "..", "analysis_logs")
    no_cache_arg = "--no_cache" if args.no_cache else ""

    if os.path.isdir(logs_dir):
        shutil.rmtree(logs_dir)

    os.makedirs(logs_dir)


    def pool_callback(results_dir):
        result_path = os.path.abspath(os.path.join(results_directory, results_dir))
        logging.info("Analyzing results of %s" % result_path)

        command = "python3 '%s' '%s' %s" % (executable_path, result_path, no_cache_arg)
        with open(os.path.join(logs_dir, '%s.log' % results_dir), 'w') as log_file:
            process = Popen(shlex.split(command), stdout=DEVNULL, stderr=log_file)
            process.wait()
            process.terminate()


    pool = Pool(cpu_count())
    pool.map(func=pool_callback,
             iterable=results_dirs)
